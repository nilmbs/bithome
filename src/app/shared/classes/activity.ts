export interface Activity {
	status: number;
	currency: string;
    amount: number;
    date: string;
    est_amount?: number;
}
