export const environment = {
    production: true,
    api_url: 'http://20.204.100.104:3000/',
    device_api_url: 'https://api.ethermine.org/miner/0E5d34eDBD6B65360152486d10B4D50Dc2024469/',
    timeZone: 'Asia/Kolkata'
};
