import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { FuseConfigService } from '@fuse/services/config';
import { FuseValidators } from '@fuse/validators';
import { AppConfig } from 'app/core/config/app.config';
import { UserService } from 'app/core/user/user.service';
import { AuthService } from 'app/core/auth/auth.service';
import { MinerService } from 'app/shared/services/miner.service';

@Component({
    selector     : 'auth-sign-up',
    templateUrl  : './sign-up.component.html',
    styleUrls: ["./sign-up.component.scss"],
    //encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignUpComponent implements OnInit
{
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signUpForm: FormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _fuseConfigService: FuseConfigService,
        private _authService: AuthService,
        private _userService: UserService,
        private _minerService: MinerService
    ) {
    }

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signUpForm = this._formBuilder.group({
                name      : ['', Validators.required],
                email     : ['', [Validators.required, Validators.email]],
                password  : ['', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*]).{8,}$')]],
                cpassword : [''],
                agree: [false, Validators.requiredTrue]
            }, { 
              validator: FuseValidators.mustMatch('password', 'cpassword')
            }
        );
    }

    get config(): AppConfig {
        return this._fuseConfigService.config;
    }

    /**
     * Sign up
     */
    signUp(): void
    {
        // Do nothing if the form is invalid
        if ( this.signUpForm.invalid ) {
            this.signUpForm.get('agree').markAsTouched();
            return;
        }

        this.signUpForm.disable();
        this.showAlert = false;
        let formdata = this.signUpForm.value;
        let mac_id = Math.floor(Math.random() * (999999 - 100000)) + 100000;
        let postdata = {
            username: formdata.name,
            email: formdata.email,
            password: formdata.password,
            mac_id: mac_id
        }

        this._authService.signUp(postdata)
            .subscribe(resp => {
                this.signUpForm.enable();
                //this.signUpForm.reset();
                this.showAlert = true;
                if (resp.message) {
                    this.alert = {
                        type   : 'error',
                        message: resp.message
                    };
                } else {
                    if (resp.token) {
                        this._authService.loginAfterRegister(resp);
                        let user = this._authService.user;
                        user.token = this._authService.accessToken;
                        this._userService.user = user;
                        this._minerService.miner = user;
                        this._router.navigateByUrl('/dashboard');
                    }
                    this.alert = {
                        type   : 'success',
                        message: 'Registration successfull.'
                    };                    
                    this._router.navigateByUrl('/sign-in');
                }
            },
            error => {
                this.signUpForm.enable();
                this.showAlert = true;
                this.alert = {
                    type   : 'error',
                    message: 'Something went wrong, please try again.'
                };
            }
        );
    }
}
