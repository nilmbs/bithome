import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';

import { HttpService } from 'app/shared/services/http.service';

@Injectable({
    providedIn: 'root'
})

export class WalletService
{
    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpService,
        private _http: HttpClient
    ) {
    }

    /**
     * List all Wallet
     *
     * @param customer_id
     */
    getAll(customer_id: string = ''): Observable<any>
    {
        return this._http.get('api/wallet/addresses').pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    /**
     * Add Wallet
     *
     * @param customer_id
     * @param wallet_name
     * @param wallet_addr
     */
    addWallet(customer_id: string, wallet_name: string, wallet_addr: string): Observable<any>
    {
        return of(true);
        let post_data = {customer_id: customer_id, wallet_name: wallet_name, wallet_addr: wallet_addr};
        return this._httpClient.post(`wallet/add`, post_data).pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    /**
     * Update Wallet
     *
     * @param wallet_id
     * @param wallet_name
     * @param wallet_addr
     */
    updateWallet(wallet_id: string, wallet_name: string, wallet_addr: string): Observable<any>
    {
        return of(true);
        let post_data = {wallet_id: wallet_id, wallet_name: wallet_name, wallet_addr: wallet_addr};
        return this._httpClient.post(`wallet/edit`, post_data).pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    /**
     * Delete Wallet
     *
     * @param wallet_id
     */
    deleteWallet(wallet_id: string): Observable<any>
    {
        return of(true);
        let post_data = {wallet_id: wallet_id};
        return this._httpClient.post(`wallet/delete`, post_data).pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    /**
     * Withdraw amount to Wallet
     *
     * @param data
     */
    withdrawAmount(data: any): Observable<any>
    {
        return of(true);
        let post_data = data;
        return this._httpClient.post(`wallet/withdraw`, post_data).pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    private handleError(error) {
        let errorMessage = 'Something went wrong, please try again.';
        if (error.error && error.error.hasOwnProperty('message')) {
            errorMessage = error.error.message;
        } else if (error.status != 404 && error.message) {
            errorMessage = error.message;
        }

        return throwError(errorMessage);
    }
}
