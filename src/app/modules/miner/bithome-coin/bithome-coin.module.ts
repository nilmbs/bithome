import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { bithomeCoinRoutes } from './bithome-coin.routing';
import { SharedModule } from 'app/shared/shared.module';
import { BithomeCoinComponent } from './bithome-coin.component';
import { DepositModalComponent } from './modal/deposit/default.component';
import { WithdrawModalComponent } from './modal/withdraw/default.component';
import { CompoundModalComponent } from './modal/compound/default.component';
import { StakeModalComponent } from './modal/stake/default.component';
import { UnstakeModalComponent } from './modal/unstake/default.component';

@NgModule({
    declarations: [
        BithomeCoinComponent,
        DepositModalComponent,
        WithdrawModalComponent,
        CompoundModalComponent,
        StakeModalComponent,
        UnstakeModalComponent
    ],
    imports     : [
        RouterModule.forChild(bithomeCoinRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSliderModule,
        MatButtonToggleModule,
        FuseAlertModule,
        MatProgressSpinnerModule,
        SharedModule,
    ]
})
export class BithomeCoinModule
{
}
