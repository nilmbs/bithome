import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { historyRoutes } from './history.routing';
import { SharedModule } from 'app/shared/shared.module';
import { HistoryComponent } from './history.component';

@NgModule({
    declarations: [
        HistoryComponent
    ],
    imports     : [
        RouterModule.forChild(historyRoutes),
        MatButtonToggleModule,
        MatProgressSpinnerModule,
        NgApexchartsModule,
        SharedModule
    ]
})

export class HistoryModule {
}
