import { Route} from '@angular/router';

import { AccountComponent } from './account.component';

export const accountRoutes: Route[] = [
    {
        path     : '',
        component: AccountComponent
    }
];

