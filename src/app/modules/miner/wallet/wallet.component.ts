import { Component, OnInit } from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import * as moment from 'moment';

import { MinerService } from 'app/shared/services/miner.service';
import { Activity } from 'app/shared/classes/activity';

@Component({
  	selector: 'app-wallet',
  	templateUrl: './wallet.component.html',
  	styleUrls: ['./wallet.component.scss']
})

export class WalletComponent implements OnInit {
	allActivities: Activity[] = [];
    recentActivities: Activity[] = [];
    currPage: number = 1;
    loadMore: boolean = false;
    loadingMore: boolean = false;
    showRALoader: boolean = true;
    raCurrency: '' | 'USDC' | 'HOME' = '';

  	constructor(private _minerService: MinerService) {}

  	ngOnInit(): void {
  		this.getAllActivities();
  	}

    filterRecentActivity(change: MatButtonToggleChange) {
        this.raCurrency = change.value;
        //this.currPage = 1;
        this.getRecentActivity();
    }

  	getAllActivities() {
        this._minerService.getRecentActivity(this.currPage).subscribe(resp => {
            resp.data.forEach((item, index) => {
                const aitem = item.earning;
                let activity = {
                    status: (aitem.value=='add'?1:2),
                    currency: aitem.currency,
                    amount: aitem.earning,
                    date: moment.unix(aitem.endDate).format('MMMM DD, YYYY'),
                    est_amount: (aitem.currency=='HOME'?aitem.earning*.05:aitem.earning)
                };
                this.allActivities.push(activity);
            });
            this.getRecentActivity();
            this.showRALoader = false;
            this.loadMore = false;
            if (resp.data.length > 0) this.loadMore = true;
        });
    }

    getRecentActivity() {
        if (this.raCurrency == '') {
            this.recentActivities = this.allActivities;
        } else {
            let filtered = this.allActivities.filter(item => item.currency == this.raCurrency);
            this.recentActivities = filtered;
        }

        this.loadingMore = false;
    }

    loadMoreActivities() {
        this.loadingMore = true;
        this.currPage += 1;
        this.getAllActivities();
    }
}
