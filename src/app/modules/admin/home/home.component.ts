import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatButtonToggleChange } from '@angular/material/button-toggle';

import { AdminMinerService } from 'app/shared/services/admin/miner.service';
import { User } from 'app/core/user/user.types';

@Component({
  	selector: 'app-admin-home',
  	templateUrl: './home.component.html',
  	styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
    datePipe: DatePipe = new DatePipe('en-US');

    private searchTimeout: any = null;
    keyword: string = '';
    allminers: User[] = [];
    filteredMiners: User[] = [];
    miners: User[] = [];
    listType: 'daily' | 'weekly' | 'monthly' = 'daily';
    totalPage: number = 0;
    currPage: number = 1;
    itemLimit: number = 5;
    loadMore: boolean = false;

  	constructor(
        private _minerService: AdminMinerService
  	) { }

  	ngOnInit(): void {
  		this.getAllMiners();
  	}

    get currDayMonth()
    {
        let date = new Date();
        let month = this.datePipe.transform(date, 'MMM');
        let day = this.datePipe.transform(date, 'dd');
        return month + '<span>' + day + '</span>';
    }

    getAllMiners() {
        this._minerService.getList().subscribe(resp => {
            this.totalPage = Math.ceil(resp.length / this.itemLimit);
            this.allminers = resp;
            this.filteredMiners = resp;
            this.getMiners();
        });
    }

    getMiners() {
        let limit = this.currPage * this.itemLimit;
        this.miners = this.filteredMiners.slice(0, limit);
        this.loadMore = false;
    }

    searchMiners(evt: any) {
        clearTimeout(this.searchTimeout);
        let $this = this;
        this.keyword = evt.target.value;
        this.searchTimeout = setTimeout(() => {
            if (evt.keyCode != 13) {
                $this.findByKeyword();
            }
        }, 1000);
    }

    findByKeyword() {
        this.filteredMiners = this.allminers.filter(miner => miner.name.toLowerCase().includes(this.keyword.toLowerCase()));
        this.currPage = 1;
        this.totalPage = Math.ceil(this.filteredMiners.length / this.itemLimit);
        this.getMiners();
    }

    changeListType(change: MatButtonToggleChange) {
        this.listType = change.value;
        this.keyword = '';
        this.getAllMiners();
    }

    loadMoreMiners() {
        if (this.totalPage > this.currPage) {
            this.loadMore = true;
            this.currPage += 1;
            setTimeout(() => {
                this.getMiners();
            }, 1000);
        }
    }

    minersSortBy(skey: string, dir: string) {
        let direction = dir.toUpperCase();
        if (direction === 'ASC') {
            this.filteredMiners = this.filteredMiners.sort((a, b) => {
                if (a[skey] < b[skey]) {
                    return -1;
                } else if (a[skey] > b[skey]) {
                    return 1;
                }
                return 0;
            })
        } else if (direction === 'DESC') {
            this.filteredMiners = this.filteredMiners.sort((a, b) => {
                if (a[skey] > b[skey]) {
                    return -1;
                } else if (a[skey] < b[skey]) {
                    return 1;
                }
                return 0;
            })
        }
        //this.currPage = 1;
        this.getMiners();
    }
}
