export interface BithomeCoinData {
	balance: number;
	total_earn: number;
	staked: number;
	unstaked_earn: number;
	total_staked: number;
	apy: string;
	recent_profit: number;
	total_profit: number;
}

export let DefaultBthCoinData = {
	balance: 0,
	total_earn: 0,
	staked: 0,
	unstaked_earn: 0,
	total_staked: 0,
	apy: '0%',
	recent_profit: 0,
	total_profit: 0
}
