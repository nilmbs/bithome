/* eslint-disable */
import * as moment from 'moment';

export const histories = {
    total_earn : {
        '30days'  : 2458.4534,
        'alltime' : 270780.4563
    },
    avg_profit        : {
        hourly : {
            values : [0.35, 0.30, 0.45, 0.50, 0.25, 0.33, 0.44, 0.47, 0.33, 0.45, 0.20, 0.45, 0.34, 0.47, 0.50, 0.49, 0.39, 0.28, 0.25, 0.40, 0.32, 0.36, 0.34, 0.49, 0.35],
            labels : ["12:00 AM", "1:00 AM", "2:00 AM", "3:00 AM", "4:00 AM", "5:00 AM", "6:00 AM", "7:00 AM", "8:00 AM", "9:00 AM", "10:00 AM", "11:00 AM", "12:00 PM", "1:00 PM", "2:00 PM", "3:00 PM", "4:00 PM", "5:00 PM", "6:00 PM", "7:00 PM", "8:00 PM", "9:00 PM", "10:00 PM", "11:00 PM", "12:00 AM"]
        },
        daily : {
            values : [7.36, 8.91, 6.72, 8.54, 8.65, 7.03, 7.35, 6.41, 6.93],
            labels : [                
                moment().subtract(8, 'days').format('MM/DD'),
                moment().subtract(7, 'days').format('MM/DD'),
                moment().subtract(6, 'days').format('MM/DD'),
                moment().subtract(5, 'days').format('MM/DD'),
                moment().subtract(4, 'days').format('MM/DD'),
                moment().subtract(3, 'days').format('MM/DD'),
                moment().subtract(2, 'days').format('MM/DD'),
                moment().subtract(1, 'days').format('MM/DD'),
                moment().format('MM/DD')
            ]
        },
        weekly : {
            values : [38, 35, 37, 29, 30, 45, 40.46, 43, 35],
            labels : [
                moment().subtract(70, 'days').format('MM/DD') + ' - ' + moment().subtract(63, 'days').format('MM/DD'),
                moment().subtract(62, 'days').format('MM/DD') + ' - ' + moment().subtract(55, 'days').format('MM/DD'),
                moment().subtract(54, 'days').format('MM/DD') + ' - ' + moment().subtract(47, 'days').format('MM/DD'),
                moment().subtract(46, 'days').format('MM/DD') + ' - ' + moment().subtract(39, 'days').format('MM/DD'),
                moment().subtract(38, 'days').format('MM/DD') + ' - ' + moment().subtract(31, 'days').format('MM/DD'),
                moment().subtract(30, 'days').format('MM/DD') + ' - ' + moment().subtract(23, 'days').format('MM/DD'),
                moment().subtract(22, 'days').format('MM/DD') + ' - ' + moment().subtract(15, 'days').format('MM/DD'),
                moment().subtract(14, 'days').format('MM/DD') + ' - ' + moment().subtract(7, 'days').format('MM/DD'),
                moment().subtract(6, 'days').format('MM/DD') + ' - ' + moment().format('MM/DD')
            ]
        }
    }
};
