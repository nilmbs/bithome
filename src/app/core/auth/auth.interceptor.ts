import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { ToasterService } from 'app/shared/services/toaster.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor
{
    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private toasterService:ToasterService,
        private _authService: AuthService
    ) {
    }

    /**
     * Intercept
     *
     * @param req
     * @param next
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        // Clone the request object
        let newReq = req.clone();

        if ( this._authService.accessToken && !AuthUtils.isTokenExpired(this._authService.accessToken) )
        {
            newReq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + this._authService.accessToken)
            });
        }

        // Response
        return next.handle(newReq).pipe(
            catchError((error) => {

                if (error instanceof HttpErrorResponse) {
                    try {
                       if(error.error.errors[0].msg){
                            this.toasterService.openSnackBar(error.error.errors[0].msg, 'error')
                        }

                        if (error.error.errors[0].code && 'inactive' == error.error.errors[0].code) {
                            localStorage.clear()
                            this._router.navigate(['sign-in']);
                        }
                    } catch(e) {
                        if (error.status === 401) {
                            //this._authService.signOut();
                            //location.reload();
                        } else if (error.status == 404) {
                            //this.toasterService.openSnackBar('API resource not found.', 'error')
                        } else if (error.status == 0) {
                            //this.toasterService.openSnackBar('An unexpected error has occurred. API resource not working.', 'error')
                        }
                    }
                }

                return throwError(error);
            })
        );
    }
}
