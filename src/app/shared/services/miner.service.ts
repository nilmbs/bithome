import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';

import { HttpService } from 'app/shared/services/http.service';
import { User } from 'app/core/user/user.types';

@Injectable({
    providedIn: 'root'
})

export class MinerService
{
    private _miner: BehaviorSubject<User> = new BehaviorSubject<User>(null);
    private _balance: BehaviorSubject<any> = new BehaviorSubject<any>(0);

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpService,
        private _http: HttpClient
    ) {
    }

    set miner(miner: User)
    {
        this._miner.next(miner);
    }

    get miner(): User
    {
        return this._miner.value;
    }

    set balance(amount: any)
    {
        this._balance.next(amount);
    }

    get balance(): any
    {
        return this._balance.value;
    }

    /**
     * Get balance
     *
     */
    getBalance(): Observable<any>
    {
        return this._httpClient.post('earn/totalethearninterval', {email: this.miner.email}).pipe(
            map((response: any) => {
                const data = response.data.find(item => item._id == this.miner.email);
                this.balance = data ? data.earning : 0;
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Get total earnings (past 30 days, all time)
     *
     */
    getTotalEarningHistory(): Observable<any>
    {
        return this._httpClient.post('earn/totalethearninterval', {email: this.miner.email}).pipe(
            map((response: any) => {
                const data = response.data.find(item => item._id == this.miner.email);
                this.balance = data ? data.earning : 0;
                return data ? data.earning : 0;
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Get average profit (hourly, daily, weekly)
     *
     */
    getAvgProfitHistory(): Observable<any>
    {
        return forkJoin([
            this._httpClient.post('earn/dailyHourlyEarnHistory', {email: this.miner.email}).pipe(map(response => response)),
            this._httpClient.post('earn/dailyEarnHistory', {email: this.miner.email}).pipe(map(response => response)),
            this._httpClient.post('earn/weekEarnHistory', {email: this.miner.email}).pipe(map(response => response)),
            //this._httpClient.post('earn/hourly', {email: this.miner.email}).pipe(map(response => response))
        ]);
    }

    /**
     * Get daily profit in dashboard
     *
     */
    getDailyProfitDashboard(): Observable<any>
    {
        return this._httpClient.post('earn/dailyWeekEarnDash', {email: this.miner.email}).pipe(
            map(response => response),
            catchError(this.handleError)
        );
    }

    /**
     * Get recent activity
     *
     */
    getRecentActivity(page: number = 1): Observable<any>
    {
        return this._httpClient.post('earn/ethgetinterval', {email: this.miner.email, page: page}).pipe(
            map(response => response),
            catchError(this.handleError)
        );
    }

    /**
     * Get wallet recent activity
     *
     */
    getEarningRecentActivity(): Observable<any>
    {
        return this._http.get('api/common/activities').pipe(
            map((response: any) => response.wallet),
            catchError(this.handleError)
        );
    }

    /**
     * Change password
     *
     * @param data
     */
    changePassword(data: {token: string, email: string, curr_password: string, password: string}): Observable<any>
    {
        let postdata = {email: data.email, password: data.curr_password, new_password: data.password};
        return this._httpClient.post(`auth/change-password/${data.email}/${data.token}`, postdata).pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    /**
     * Update miner data
     *
     * @param minerdata
     */
    updateMiner(minerdata): Observable<any>
    {
        return this._httpClient.post('auth/update-miner', minerdata).pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    private handleError(error) {
        let errorMessage = 'Something went wrong, please try again.';
        if (error.error && error.error.hasOwnProperty('message')) {
            errorMessage = error.error.message;
        } else if (error.status != 404 && error.message) {
            errorMessage = error.message;
        }

        return throwError(errorMessage);
    }
}
