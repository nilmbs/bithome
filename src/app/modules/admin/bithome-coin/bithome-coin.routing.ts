import { Route} from '@angular/router';

import { BithomeCoinComponent } from './bithome-coin.component';

export const bithomeCoinRoutes: Route[] = [
    {
        path     : '',
        component: BithomeCoinComponent
    }
];

