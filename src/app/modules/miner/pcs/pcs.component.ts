import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { merge } from 'lodash-es';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { FuseAlertType } from '@fuse/components/alert';
import * as moment from 'moment';

import { PcsService } from 'app/shared/services/pcs.service';
import { CommonUtils } from 'app/shared/utils/common.utils';
import { AuthService } from 'app/core/auth/auth.service';
import { Device } from 'app/shared/classes/device';
import { ToasterService } from 'app/shared/services/toaster.service';
import { AddPcModalComponent} from "app/shared/components/modal/add-pc/add-pc.component";
import { PcschedulingModalComponent } from './modal/scheduling/default.component';

@Component({
    selector: 'app-pcs',
    templateUrl: './pcs.component.html',
    styleUrls: ['./pcs.component.scss']
})

export class PcsComponent implements OnInit, OnDestroy {
    @ViewChild("addPcModal") addPcModal: AddPcModalComponent;
    @ViewChild("schedulingModal") schedulingModal: PcschedulingModalComponent;

    commonUtils = CommonUtils;
    private _unsubsPcAll: Subject<any> = new Subject<any>();
    private _unsubsPcStats: Subject<any> = new Subject<any>();
    all_pcs: Device[] = [];
    totalPc: number = 0;
    showLoader: boolean = true;
    private deviceExtKeys: any = {
        hash_rate: 0,
        estimate_daily: 0,
        estimate_weekly: 0,
        estimate_monthly: 0,
        last_seen: 0,
        temperature: '0°C',
        cpu_load: '0%',
        scheduling: false,
        game_detection: false,
        is_optimized: false
    };

    constructor(
        private _toasterService:ToasterService,
        private _authService: AuthService,
        private _pcsService: PcsService
    ) { }

    ngOnInit() {
        this.getAllPcs();
        /*this._pcsService.checkNewPcAdded().subscribe(
            (resp) => {
                //console.log(resp)
                if (!!resp.device) {
                    let index = this.all_pcs.findIndex((d:Device) => d.device_id == resp.device.device_id);
                    if (index < 0){
                        index = this.all_pcs.length;
                        let nitem: Device = resp.device;
                        let itemext = {
                            hash_rate: 2537495678,
                            estimate_daily: 25.678,
                            estimate_weekly: 25.678 * 7,
                            estimate_monthly: 750.564,
                            last_seen: 0,
                            temperature: '65°C',
                            cpu_load: '87%',
                            scheduling: true,
                            game_detection: false
                        };
                        nitem = merge(itemext, nitem);
                        this.all_pcs[index] = nitem;
                        this.totalPc = this.all_pcs.length;
                    }
                }
            },
            (error) => {
            }
        );*/
    }

    getAllPcs() {
        let user = this._authService.user;
        this._pcsService.getAll(user.email, this._authService.accessToken).subscribe(
            (resp) => {
                this.all_pcs = resp.devices;
                this.totalPc = this.all_pcs.length;
                if (this.totalPc <= 0) {
                    this.showLoader = false;
                    return;
                }
                this.all_pcs.filter((item: Device, index) => {
                    item = merge(item, this.deviceExtKeys);
                });
                this._pcsService.getAll(user.email, this._authService.accessToken, true)
                    .pipe(takeUntil(this._unsubsPcAll))
                    .subscribe((resp) => {
                        resp.devices.filter((item: Device, index) => {
                            const pcindex = this.all_pcs.findIndex(p => p.device_id == item.device_id);
                            let pcitem = this.all_pcs[pcindex];
                            if (pcitem) {
                                pcitem = merge(pcitem, item);
                                if (this.pcAppSts(item) != 0 && item.is_active == 0) {
                                    delete this.deviceExtKeys.last_seen;
                                    pcitem = merge(pcitem, this.deviceExtKeys);
                                }
                                this.all_pcs[pcindex] = pcitem;
                            }
                        });
                });
                this._pcsService.getAllPcStats(user.email, 600)
                    .pipe(takeUntil(this._unsubsPcStats))
                    .subscribe(
                        (stsresp) => {
                            //console.log(stsresp.data)
                            this.all_pcs.filter((item: Device, index) => {
                                let pcsts = stsresp.data.find(w => w.worker == item.device_id);
                                this.updatePcsStats(pcsts, item);
                                this._pcsService.getStats(item.device_id).subscribe(
                                    (resp) => {
                                        //console.log(resp)
                                        if (resp.status != 'OK' || typeof resp.data != 'object') return;
                                        this.updatePcsStats(resp.data, item, true);
                                    },
                                    (error) => {
                                    }
                                );
                            });
                            this.showLoader = false;
                        },
                        (error) => {
                            this.showLoader = false;
                        }
                    );
            },
            (error) => {
                this.showLoader = false;
                //this._toasterService.openSnackBar(error, 'error')
            }
        );
    }

    get activeCount() {
        return this.all_pcs.reduce((acnt, pc:any) => {
            if (pc.is_active == 1) acnt++;
            return acnt;
        }, 0);
    }

    changeScheduling(evt: any, pc): void
    {
        const index = this.all_pcs.indexOf(pc);
        if (evt.target.checked) this.all_pcs[index].scheduling = true;
        else this.all_pcs[index].scheduling = false;
    }

    changeGameDetection(evt: any, pc): void
    {
        const index = this.all_pcs.indexOf(pc);
        if (evt.target.checked) this.all_pcs[index].game_detection = true;
        else this.all_pcs[index].game_detection = false;
    }

    changeOptimizedMining(evt: any, pc): void
    {
        const index = this.all_pcs.indexOf(pc);
        if (evt.target.checked) this.all_pcs[index].is_optimized = true;
        else this.all_pcs[index].is_optimized = false;
    }

    chkIsMining(device) {
        return device.is_app != 0 && device.is_active == 1;
    }

    pcAppSts(device) {
        let appsts = device.is_app || 0;
        return appsts;
    }

    startMining(device: any) {
        if (this.pcAppSts(device) == 0) return;
        this._pcsService.saveStatus(this._authService.user.email, device.device_id, 1).subscribe(
            (resp) => {
                if (resp.status == 1) {
                    this.all_pcs.filter((item: Device, index) => {
                        if (item.device_id === device.device_id) {
                            item.is_active = 1;
                        }
                    });
                    this._toasterService.openSnackBar('Mining started successfully.', 'success');
                }
            },
            (error) => {
                //this._toasterService.openSnackBar(error, 'error')
            }
        );
    }

    stopMining(device: Device) {
        if (this.pcAppSts(device) == 0) return;
        this._pcsService.saveStatus(this._authService.user.email, device.device_id, 2).subscribe(
            (resp) => {
                if (resp.status == 2) {
                    this.all_pcs.filter((item: Device, index) => {
                        if (item.device_id === device.device_id) {
                            item.is_active = 0;
                        }
                    });
                    this._toasterService.openSnackBar('Mining stopped successfully.', 'success');
                }
            },
            (error) => {
                //this._toasterService.openSnackBar(error, 'error')
            }
        );
    }

    expandPcDetails($event: NgbPanelChangeEvent) {
        let pcidx = $event.panelId.replace(/pcrow-/, '');
        const panelElem = document.getElementById('pc-row-' + pcidx);
        panelElem.classList.remove("expanded");
        if ($event.nextState)
            panelElem.classList.add("expanded");
    }

    addPc() {
        let user = this._authService.user;
        let postdata = {
            email: user.email,
            mac_id: Math.floor(Math.random() * (999999 - 100000)),
            pc_name: 'PC ' + Math.floor(Math.random() * (99 - 10)),
            gpu: 'AMD Radeon RX 5700 XT 8GB',
            coin: 'Ethereum'
        };
        this._pcsService.registerPC(postdata).subscribe(
            (resp) => {
                this.getAllPcs();
            },
            (error) => {
                
            }
        );
    }

    private updatePcsStats(pcsts, item, single = false) {
        if (!pcsts) {
            item.is_active = 0;
            item.temperature = '0°C';
            item.cpu_load = '0%';
            item.hash_rate = 0;
            item.scheduling = false;
            item.game_detection = false;
            item.is_optimized = false;
            if (!single) {
                item.estimate_daily = 0;
                item.estimate_weekly = 0;
                item.estimate_monthly = 0;
            }
            return;
        }
        let itemActive = false;
        const rhashRate = pcsts.reportedHashrate || 0;
        if (+item.last_seen > 0) {
            let prevLastSeen = moment.unix(+item.last_seen);
            let lastSeen = moment.unix(+pcsts.lastSeen);
            if (lastSeen.diff(prevLastSeen, 'minutes') > 15) {
                itemActive = false;
            } else if (rhashRate > 0) {
                itemActive = true;
            }
        }
        if (rhashRate > 0 && +item.last_seen == 0) itemActive = true;
        //if (rhashRate <= 0 && item.is_active == 1) item.is_active = 0;
        if (itemActive) {
            item.temperature = '65°C';
            item.cpu_load = '87%';
            item.scheduling = true;
            item.game_detection = false;
            item.is_optimized = false;
            item.hash_rate = pcsts.currentHashrate || 0;
            if (!single) {
                item.estimate_daily = pcsts.estimateDay || 0;
                item.estimate_weekly = (pcsts.estimateDay || 0) * 7;
                item.estimate_monthly = pcsts.estimateMonth || 0;
            }
        } else {
            item.temperature = '0°C';
            item.cpu_load = '0%';
            item.hash_rate = 0;
            item.scheduling = false;
            item.game_detection = false;
            item.is_optimized = false;
            if (!single) {
                item.estimate_daily = 0;
                item.estimate_weekly = 0;
                item.estimate_monthly = 0;
            }
        }
        if (!!pcsts.lastSeen) item.last_seen = pcsts.lastSeen;
    }

    ngOnDestroy() {
        this._unsubsPcAll.next();
        this._unsubsPcAll.complete();
        this._unsubsPcStats.next();
        this._unsubsPcStats.complete();
        if ('newpcadded' in this._pcsService.pcEventSources)
            this._pcsService.pcEventSources['newpcadded'].close();
    }
}
