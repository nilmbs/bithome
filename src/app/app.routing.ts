import { Route } from '@angular/router';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { NoAuthGuard } from 'app/core/auth/guards/noAuth.guard';
import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';

// @formatter:off
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
export const appRoutes: Route[] = [

    // Redirect empty path to '/dashboards/project'
    {path: '', pathMatch : 'full', redirectTo: 'dashboard'},

    // Redirect signed in user to the '/dashboards/project'
    //
    // After the user signs in, the sign in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    {path: 'signed-in-redirect', pathMatch : 'full', redirectTo: 'dashboards/project'},

    // Auth routes for guests
    {
        path: '',
        //canActivate: [NoAuthGuard],
        //canActivateChild: [NoAuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            {
                path: 'forgot-password',
                loadChildren: () => import('app/modules/auth/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule)
            },
            {
                path: 'reset-password',
                loadChildren: () => import('app/modules/auth/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule)
            },
            {
                path: 'sign-in',
                canActivate: [NoAuthGuard],
                loadChildren: () => import('app/modules/auth/sign-in/sign-in.module').then(m => m.AuthSignInModule)
            },
            {
                path: 'sign-up',
                canActivate: [NoAuthGuard],
                loadChildren: () => import('app/modules/auth/sign-up/sign-up.module').then(m => m.AuthSignUpModule)
            }
        ]
    },

    // Auth routes for authenticated users
    {
        path: '',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            {path: 'sign-out', loadChildren: () => import('app/modules/auth/sign-out/sign-out.module').then(m => m.AuthSignOutModule)},
            {path: 'unlock-session', loadChildren: () => import('app/modules/auth/unlock-session/unlock-session.module').then(m => m.AuthUnlockSessionModule)}
        ]
    },

    // Landing routes
    {
        path: '',
        component  : LayoutComponent,
        data: {
            layout: 'empty'
        },
        children   : [
            {path: 'home', loadChildren: () => import('app/modules/landing/home/home.module').then(m => m.LandingHomeModule)},
        ]
    },

    // Admin routes
    {
        path         : 'admin',
        loadChildren : () => import('./modules/admin/admin.module').then(m => m.AdminModule)
    },

    // Pages routes
    {
        path         : 'pages',
        component    : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        loadChildren : () => import('./modules/pages/pages.module').then(m => m.PagesModule)
    },

    // Miner routes
    {
        path       : '',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {
                path: 'dashboard',
                loadChildren: () => import('./modules/miner/dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'pcs',
                loadChildren: () => import('./modules/miner/pcs/pcs.module').then(m => m.PcsModule)
            },
            {
                path: 'history',
                loadChildren: () => import('./modules/miner/history/history.module').then(m => m.HistoryModule)
            },
            {
                path: 'earnings',
                loadChildren: () => import('./modules/miner/earnings/earnings.module').then(m => m.EarningsModule)
            },
            {
                path: 'wallet',
                loadChildren: () => import('./modules/miner/wallet/wallet.module').then(m => m.WalletModule)
            },
            {
                path: 'account',
                loadChildren: () => import('./modules/miner/account/account.module').then(m => m.AccountModule)                
            },
            {
                path: 'bithome-coin',
                loadChildren: () => import('./modules/miner/bithome-coin/bithome-coin.module').then(m => m.BithomeCoinModule)                
            },
        ]
    },
    // 404 & Catch all
    {
        path: '**',
        redirectTo: 'pages/error/404'
    }
];
