import { NgModule } from '@angular/core';
import {MatTabsModule} from '@angular/material/tabs';
import { RouterModule } from '@angular/router';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { dashboardRoutes } from './dashboard.routing';
import { SharedModule } from 'app/shared/shared.module';
import { DashboardComponent } from './dashboard.component';


@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports     : [
        RouterModule.forChild(dashboardRoutes),
        NgApexchartsModule,
        MatProgressSpinnerModule,
        SharedModule,
        MatTabsModule
    ]
})

export class DashboardModule {
}
