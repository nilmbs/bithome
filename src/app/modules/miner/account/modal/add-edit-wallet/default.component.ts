import { Component, OnInit, OnDestroy, ViewChild,
    TemplateRef, PLATFORM_ID, Inject, Input, Output, EventEmitter } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { ToasterService } from 'app/shared/services/toaster.service';
import { FuseAlertType } from '@fuse/components/alert';
import { WalletService } from 'app/shared/services/wallet.service';

@Component({
    selector: 'add-edit-wallet-modal',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})
export class AddEditWalletModalComponent implements OnInit, OnDestroy {
    @ViewChild("addEditWalletModal", { static: false }) addEditWalletModal: TemplateRef<any>;

    @Input() wallets: any[] = [];
    @Output() addOrEdit: EventEmitter<any> = new EventEmitter<any>();

    public closeResult: string;
    public modalOpen: boolean = false;
    showAlert: boolean = false;
    walletForm: FormGroup;
    isEdit: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    prevwcnt: number = 0;

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private toasterService:ToasterService,
        private _walletService: WalletService
    ) { }

    ngOnInit(): void {
        this.walletForm = this.formBuilder.group({
            wallet_id: [''],
            wallet_name: ['', [Validators.required]],
            wallet_addr: ['', [Validators.required]],
        });
    }

    saveWallet() {
        if ( this.walletForm.invalid ) {
            return;
        }

        this.walletForm.disable();
        this.showAlert = false;
        var data = this.walletForm.getRawValue();

        let chkwallet = this.wallets.reduce((cnt, w) => {
            if (this.isEdit && w.address == data.wallet_addr && w.wallet_id != data.wallet_id) cnt++;
            else if (!this.isEdit && w.address == data.wallet_addr) cnt++;
            return cnt;
        }, 0);
        if (chkwallet > 0) {
            this.showAlert = true;
            this.alert = {
                type   : 'error',
                message: 'Wallet address already exists.'
            };
            this.walletForm.enable();
            return;
        }

        let walletResult = null;
        if (this.isEdit) walletResult = this._walletService.updateWallet('','','');
        else walletResult = this._walletService.addWallet('','','');

        walletResult.pipe(
                finalize(() => {
                    this.walletForm.enable();
                    this.showAlert = true;
                })
            )
            .subscribe(
                (resp) => {
                    this.walletForm.reset();
                    this.toasterService.openSnackBar('Wallet '+(this.isEdit?'updated':'added')+' successfully.', 'success');
                    let result: any = {added: true, wallet: {nickname: data.wallet_name, address: data.wallet_addr}};
                    if (this.isEdit) {
                        delete result.added;
                        result.updated = true;
                        result.wallet.wallet_id = data.wallet_id;
                    } else this.prevwcnt++;
                    this.addOrEdit.emit(result);
                    this.modalService.dismissAll();
                },
                (error) => {
                    this.alert = {
                        type   : 'error',
                        message: error
                    };
                }
            );
    }

    openModal(wallet: any = '') {
        if (this.prevwcnt == 0) this.prevwcnt = this.wallets.length;
        this.showAlert = false;
        this.isEdit = false;
        this.walletForm.reset();
        if (wallet) {
            this.isEdit = true;
            this.walletForm.get('wallet_id').setValue(wallet.wallet_id);
            this.walletForm.get('wallet_name').setValue(wallet.nickname);
            this.walletForm.get('wallet_addr').setValue(wallet.address);
        } else {
            this.walletForm.get('wallet_name').setValue('CoinBase '+(this.prevwcnt+1));
            this.walletForm.get('wallet_addr').setValue(this.generateWalletAddress());
        }

        this.modalOpen = true;
        if (isPlatformBrowser(this.platformId)) {
            this.modalService.open(this.addEditWalletModal, { 
                size: 'lg',
                ariaLabelledBy: 'addEditWallet-Modal',
                centered: true,
                windowClass: 'modal fade add-edit-wallet' 
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if(this.modalOpen){
            this.modalService.dismissAll();
        }
    }

    generateWalletAddress(length = 35) {
        var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var result = '';
        for ( var i = 0; i < length; i++ ) {
            result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
        }

        return result;
    }
}
