import { Route} from '@angular/router';

import { EarningsComponent } from './earnings.component';

export const earningRoutes: Route[] = [
    {
        path     : '',
        component: EarningsComponent
    }
];

