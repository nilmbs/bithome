import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash-es';
import { FuseMockApiService } from '@fuse/lib/mock-api';
import { data as newPcData } from './data';

@Injectable({
    providedIn: 'root'
})
export class DevicesMockApi
{
    private _data: any = newPcData;

    /**
     * Constructor
     */
    constructor(private _fuseMockApiService: FuseMockApiService)
    {
        // Register Mock API handlers
        this.registerHandlers();
    }

    /**
     * Register Mock API handlers
     */
    registerHandlers(): void
    {
        this._fuseMockApiService
            .onGet('api/new-pc')
            .reply(() => [200, cloneDeep(this._data)]);
    }
}
