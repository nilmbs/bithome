import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector	  : 'app-page-terms-condition',
  templateUrl : './default.component.html',
  styleUrls   : ['./default.component.scss']
})

export class TermsConditionComponent implements OnInit {

  constructor(
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }
}
