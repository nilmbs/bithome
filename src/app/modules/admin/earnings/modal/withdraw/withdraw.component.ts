import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { FuseValidators } from '@fuse/validators';

import { FuseAlertType } from '@fuse/components/alert';
import { WalletService } from 'app/shared/services/wallet.service';

@Component({
  selector: 'withdraw-earning-modal',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})

export class WithdrawEarningModalComponent implements OnInit, OnDestroy {

    @ViewChild("withdrawModal", { static: false }) withdrawModal: TemplateRef<any>;

    public closeResult: string;
    public modalOpen: boolean = false;
    showAlert: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    withdrawalForm: FormGroup;
    isNewWallet: boolean = false;
    isSuccess: boolean = false;
    walletAddrs: any[] = [{name:'Wallet Address 1', addr: 'WALLETADDRESS1'}, {name:'Wallet Address 2', addr: 'WALLETADDRESS2'}];

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private _walletService: WalletService
    ) { }

    ngOnInit(): void {
        this.withdrawalForm = this.formBuilder.group({
            amount: ['', [Validators.required, FuseValidators.number()]],
            wallet_id: [''],
            wallet_name: [''],
            wallet_addr: [''],
            remember: [false]
        });
    }

    newWallet(isnew: boolean = false) {
        if (isnew) {
            this.isNewWallet = true;
            this.withdrawalForm.get('wallet_id').setValidators(null);
            this.withdrawalForm.get('wallet_name').setValidators([Validators.required]);
            this.withdrawalForm.get('wallet_addr').setValidators([Validators.required]);
            this.withdrawalForm.get('wallet_id').updateValueAndValidity();
        } else {
            this.isNewWallet = false;
            this.withdrawalForm.get('wallet_id').setValidators([Validators.required]);
            this.withdrawalForm.get('wallet_name').setValidators(null);
            this.withdrawalForm.get('wallet_addr').setValidators(null);
            this.withdrawalForm.get('wallet_id').updateValueAndValidity();
        }
    }

    sendToWallet() {console.log(this.withdrawalForm.get('amount'))
        if ( this.withdrawalForm.invalid ) {
            return;
        }
        this.isSuccess = true;
        return;
        this.withdrawalForm.disable();
        this.showAlert = false;
        var formdata = this.withdrawalForm.value;
        console.log(formdata);
        this._walletService.addWallet('','','')
            .pipe(
                finalize(() => {
                    this.withdrawalForm.enable();
                    this.showAlert = true;
                })
            )
            .subscribe(
                (resp) => {
                    this.withdrawalForm.reset();
                    this.alert = {
                        type   : 'success',
                        message: 'Wallet added successfully.'
                    };
                },
                (error) => {
                    this.alert = {
                        type   : 'error',
                        message: error
                    };
                }
            );
    }

    openModal() {
        this.isSuccess = false;
        this.withdrawalForm.reset();
        this.newWallet();
        this.withdrawalForm.get('wallet_id').setValue('');
        this.modalOpen = true;

        if (isPlatformBrowser(this.platformId)) { // For SSR 
            this.modalService.open(this.withdrawModal, { 
                size: 'lg',
                ariaLabelledBy: 'withdrawEarning-Modal',
                centered: true,
                windowClass: 'modal fade withdraw-earning' 
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if(this.modalOpen){
            this.modalService.dismissAll();
        }
    }

}
