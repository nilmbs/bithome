import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

import { walletRoutes } from './wallet.routing';
import { SharedModule } from 'app/shared/shared.module';
import { WalletComponent} from './wallet.component';

@NgModule({
    declarations: [
        WalletComponent
    ],
    imports     : [
        RouterModule.forChild(walletRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        FuseAlertModule,
        MatProgressSpinnerModule,
        MatButtonToggleModule,
        SharedModule
    ]
})
export class WalletModule
{
}
