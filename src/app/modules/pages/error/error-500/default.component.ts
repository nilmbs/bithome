import { ChangeDetectionStrategy, OnInit, Component } from '@angular/core';

@Component({
    selector        : 'error-500',
    templateUrl     : './default.component.html',
    styleUrls       : ['./default.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

export class Error500Component implements OnInit {

    constructor()
    {
    }

    ngOnInit()
    {
    }
}
