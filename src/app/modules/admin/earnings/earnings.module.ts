import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { earningRoutes } from './earnings.routing';
import { SharedModule } from 'app/shared/shared.module';
import { EarningsComponent} from './earnings.component';

@NgModule({
    declarations: [
        EarningsComponent
    ],
    imports     : [
        RouterModule.forChild(earningRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        FuseAlertModule,
        MatProgressSpinnerModule,
        SharedModule
    ]
})
export class EarningsModule
{
}
