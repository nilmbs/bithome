import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { FuseConfigService } from '@fuse/services/config';
import { AppConfig } from 'app/core/config/app.config';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
    selector     : 'auth-forgot-password',
    templateUrl  : './forgot-password.component.html',
    styleUrls: ["./forgot-password.component.scss"],
    //encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthForgotPasswordComponent implements OnInit
{
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    forgotPasswordForm: FormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _formBuilder: FormBuilder,
        private _fuseConfigService: FuseConfigService,
        private _authService: AuthService
    ) {}

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    get config(): AppConfig {
        return this._fuseConfigService.config;
    }

    /**
     * Send the reset link
     */
    sendResetLink(): void
    {
        if ( this.forgotPasswordForm.invalid ) {
            return;
        }

        this.forgotPasswordForm.disable();
        this.showAlert = false;

        // Forgot password
        this._authService.forgotPassword(this.forgotPasswordForm.get('email').value)
            .pipe(
                finalize(() => {
                    this.forgotPasswordForm.enable();
                    this.showAlert = true;
                })
            )
            .subscribe(
                (resp) => {
                    this.forgotPasswordForm.reset();
                    this.alert = {
                        type   : 'success',
                        message: 'Password reset link has been sent! Kindly please check your email.'
                    };
                },
                (error) => {
                    this.alert = {
                        type   : 'error',
                        message: error || 'Something went wrong, please try again.'
                    };
                }
            );
    }
}
