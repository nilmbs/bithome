import { InjectionToken } from '@angular/core';

export const FUSE_APP_CONFIG = new InjectionToken<any>('FUSE_APP_CONFIG');
export const FUSE_ADMIN_CONFIG = new InjectionToken<any>('FUSE_ADMIN_CONFIG');
