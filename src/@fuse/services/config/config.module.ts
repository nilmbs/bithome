import { ModuleWithProviders, NgModule } from '@angular/core';
import { FuseConfigService } from '@fuse/services/config/config.service';
import { FuseAdminConfigService } from '@fuse/services/config/admin-config.service';
import { FUSE_APP_CONFIG, FUSE_ADMIN_CONFIG } from '@fuse/services/config/config.constants';
import { adminConfig } from 'app/core/config/app.config';

@NgModule()
export class FuseConfigModule
{
    /**
     * Constructor
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseAdminConfigService: FuseAdminConfigService
    ) {
    }

    /**
     * forRoot method for setting user configuration
     *
     * @param config
     */
    static forRoot(config: any): ModuleWithProviders<FuseConfigModule>
    {
        return {
            ngModule : FuseConfigModule,
            providers: [
                {
                    provide : FUSE_APP_CONFIG,
                    useValue: config
                },
                {
                    provide : FUSE_ADMIN_CONFIG,
                    useValue: adminConfig
                }
            ]
        };
    }

    /**
     * forAdmin method for setting admin configuration
     *
     * @param config
     */
    static forAdmin(config: any): ModuleWithProviders<FuseConfigModule>
    {
        return {
            ngModule : FuseConfigModule,
            providers: [
                {
                    provide : FUSE_ADMIN_CONFIG,
                    useValue: config
                }
            ]
        };
    }
}
