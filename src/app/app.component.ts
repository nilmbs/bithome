import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import 'moment-timezone';

import { environment } from '../environments/environment';
import { IdleService } from 'app/shared/services/idle.service';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss']
})

export class AppComponent
{
    /**
     * Constructor
     */
    constructor(
    	private router: Router,
    	private dialogRef: MatDialog,
    	private modalService: NgbModal,
    	private idleService: IdleService,
    	private authService: AuthService
    ) {
        moment.tz.setDefault(environment.timeZone);
        moment.locale('en', {
            week: {
                dow: 1
            }
        });

    	if (this.authService.isLoggedIn())
    		this.idleService.isSetLastAtvtm = true;
    	this.idleService.idle$.subscribe(s => {
            let currTime = (new Date()).getTime();
            if (this.idleService.lastIdleActiveTime < currTime) {
                if (this.router.url.includes('/admin')) {
                    this.authService.signOut();
                    this.idleService.isSetLastAtvtm = false;
                    this.dialogRef.closeAll();
                    this.modalService.dismissAll();
                    this.router.navigateByUrl('/admin/sign-in');
                }
            }
    	});
    }
}
