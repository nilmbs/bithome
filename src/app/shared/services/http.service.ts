import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

@Injectable({
	providedIn: 'root'
})

export class HttpService {
  
  baseUrl = environment.api_url;

  constructor(private http:HttpClient) { } 

  get(url, payload:any = null, options:any = {}): Observable<any> {
    let params = new URLSearchParams();

    if (payload) {
      for (let key in payload) {
        if ('' !== payload[key] && null !== payload[key]) {
          params.set(key, payload[key])
        }
      }
    }

    if (!/(http(s?)):\/\//i.test(url)) url = this.baseUrl + url;

    return this.http.get(url + '?' + params.toString(), options);
  }

  post(url, data, options:any = {}): Observable<any> {
    options['headers'] = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    let postdata = new URLSearchParams();
    if (data) {
      for (let key in data) {
        if ('' !== data[key] && null !== data[key]) {
          postdata.set(key, data[key])
        }
      }
    }

    if (!/(http(s?)):\/\//i.test(url)) url = this.baseUrl + url;

    return this.http.post(url, postdata, options);
  }

  patch(url, data): Observable<any> {
    if (!/(http(s?)):\/\//i.test(url)) url = this.baseUrl + url;
    return this.http.patch(url, data);
  }

  put(url, data): Observable<any> {
    if (!/(http(s?)):\/\//i.test(url)) url = this.baseUrl + url;
    return this.http.put(url, data);
  }

  delete(url): Observable<any> {
    if (!/(http(s?)):\/\//i.test(url)) url = this.baseUrl + url;
    return this.http.delete(url);
  }

}
