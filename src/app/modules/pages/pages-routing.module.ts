import { Route } from '@angular/router';

import { Error404Component } from './error/error-404/default.component';
import { Error500Component } from './error/error-500/default.component';
import { ComingSoonComponent } from './coming-soon/default.component';
import { TermsConditionComponent } from './terms-condition/default.component';
import { PrivacyPolicyComponent } from './privacy-policy/default.component';
import { HelpSupportComponent } from './help-support/default.component';

export const pagesRoutes: Route[] = [
  { 
    path: 'error',
    data: {
      layout: 'empty'
    },
    children: [
      { 
        path: '404',
        component: Error404Component
      },
      { 
        path: '500',
        component: Error500Component
      }
    ]
  },
  { 
    path: 'coming-soon',
    data: {
      layout: 'empty'
    },
    component: ComingSoonComponent
  },
  { 
    path: 'terms-condition',
    component: TermsConditionComponent
  },
  { 
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  { 
    path: 'help-support',
    component: HelpSupportComponent
  },
];

