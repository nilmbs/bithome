import { Route } from '@angular/router';
import { AdminResetPasswordComponent } from './reset-password.component';

export const adminResetPasswordRoutes: Route[] = [
    {
        path     : ':email/:token',
        component: AdminResetPasswordComponent
    }
];
