import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {
  
  constructor(private snackBar: MatSnackBar) {
  }

  /**
   * 
   * @param message 
   * @param className | 'success' | 'error' | 'warning'
   */
  openSnackBar(message: string, className: string = 'success') {
    this.snackBar.open(message, 'Close', {
      duration: 10000,
      verticalPosition: 'bottom',
      horizontalPosition: 'center',
      panelClass: [className]
    });
  }
}
