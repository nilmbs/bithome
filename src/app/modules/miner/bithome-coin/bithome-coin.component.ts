import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FuseConfigService } from '@fuse/services/config';

import { AppConfig } from 'app/core/config/app.config';
import { DepositModalComponent } from './modal/deposit/default.component';
import { WithdrawModalComponent } from './modal/withdraw/default.component';
import { CompoundModalComponent } from './modal/compound/default.component';
import { StakeModalComponent } from './modal/stake/default.component';
import { UnstakeModalComponent } from './modal/unstake/default.component';
import { BithomeCoinService } from 'app/shared/services/bithome-coin.service';
import { BithomeCoinData } from 'app/shared/classes/bithome-coindata';
import { Activity } from 'app/shared/classes/activity';

@Component({
  	selector: 'app-bithome-coin',
  	templateUrl: './bithome-coin.component.html',
  	styleUrls: ['./bithome-coin.component.scss']
})

export class BithomeCoinComponent implements OnInit {
	@ViewChild("bitcoinDepositModal") BitcoinDepositModal: DepositModalComponent;
	@ViewChild("bitcoinWithdrawModal") BitcoinWithdrawModal: WithdrawModalComponent;
	@ViewChild("bitcoinCompoundModal") BitcoinCompoundModal: CompoundModalComponent;
	@ViewChild("bitcoinStakeModal") BitcoinStakeModal: StakeModalComponent;
    @ViewChild("bitcoinUnstakeModal") BitcoinUnstakeModal: UnstakeModalComponent;

    recentActivities: Activity[] = [];
    totalPage: number = 0;
    currPage: number = 1;
    itemLimit: number = 5;
    loadMore: boolean = false;

  	constructor(
        private _fuseConfigService: FuseConfigService,
        private _bithomeCoinService: BithomeCoinService
    ) { }

  	ngOnInit(): void {
        this.getRecentActivity();
  	}

    get config(): AppConfig {
        return this._fuseConfigService.config;
    }

    get bthdata(): BithomeCoinData {
        return this._bithomeCoinService.alldata;
    }

    setAutoStake(evt: any): void
    {
        let bthc_autostake: boolean = false;
        if (evt.target.checked) bthc_autostake = true;
        this._fuseConfigService.config = {bthc_autostake};
    }

    setAutoCompounding(evt: any): void
    {
        let bthc_autocompound: boolean = false;
        if (evt.target.checked) bthc_autocompound = true;
        this._fuseConfigService.config = {bthc_autocompound};
    }

    getRecentActivity() {
        let limit = this.currPage * this.itemLimit;
        this._bithomeCoinService.getRecentActivity().subscribe(resp => {
            this.totalPage = Math.ceil(resp.length / this.itemLimit);
            this.recentActivities = resp.slice(0, limit);
            this.loadMore = false;
        });
    }

    loadMoreActivities() {
        if (this.totalPage > this.currPage) {
            this.loadMore = true;
            this.currPage += 1;
            setTimeout(() => {
                this.getRecentActivity();
            }, 1000);
        }
    }
}
