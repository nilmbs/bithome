import { Injectable } from "@angular/core";
import { fromEvent, Observable, Subject } from "rxjs";

@Injectable({
    providedIn: 'root',
})

export class IdleService {
    private _idle: Subject<boolean> = new Subject();

    isIdle = false;
    isSetLastAtvtm = false;
    private idleAfterSeconds = 7200;
    private activityCountDown;

    constructor() {
        // Setup events
        fromEvent(document, 'mousemove').subscribe(() => this.onInteract());
        fromEvent(document, 'touchstart').subscribe(() => this.onInteract());
        fromEvent(document, 'keydown').subscribe(() => this.onInteract());
        this.onInteract();
    }

    get idle$(): Observable<any> {
        return this._idle.asObservable();
    }

    get lastActiveTime(): number {
        let actvtime = localStorage.getItem('last-active-time');
        return actvtime ? +actvtime : 0;
    }

    get lastIdleActiveTime(): number {
        return this.lastActiveTime + (this.idleAfterSeconds * 1000);
    }

    onInteract() {
        if (this.isSetLastAtvtm) {
            let dt = new Date();
            localStorage.setItem('last-active-time', dt.getTime().toString());
        }

        // Is idle and interacting, emit Wake
        if (this.isIdle) {
            this.isIdle = false;
            //this._idle.next(false);
        }

        // User interaction, reset start-idle-timer
        clearTimeout(this.activityCountDown);
        this.activityCountDown = setTimeout(() => {
            this.isIdle = true;
            this._idle.next(true);
        }, this.idleAfterSeconds * 1000)
    }

}
