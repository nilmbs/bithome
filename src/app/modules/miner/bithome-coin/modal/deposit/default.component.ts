import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { BithomeCoinService } from 'app/shared/services/bithome-coin.service';

@Component({
    selector: 'bitcoin-deposit-modal',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})
export class DepositModalComponent implements OnInit, OnDestroy {
    @ViewChild("depositModal", { static: false }) depositModal: TemplateRef<any>;

    public closeResult: string;
    public modalOpen: boolean = false;
    walletAddr: string = '0xAK378x82XC73qVSF7321e25BdFc2B82s4ad82DIJ02a';

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private _bithomeCoinService: BithomeCoinService
    ) { }

    ngOnInit(): void {
        
    }

    get balance(): number {
        return this._bithomeCoinService.balance;
    }

    openModal() {
        this.modalOpen = true;
        if (isPlatformBrowser(this.platformId)) {
            this.modalService.open(this.depositModal, { 
                size: 'lg',
                ariaLabelledBy: 'bitcoinDeposit-Modal',
                centered: true,
                windowClass: 'modal fade bitcoin-deposit-wallet' 
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if(this.modalOpen){
            this.modalService.dismissAll();
        }
    }

}
