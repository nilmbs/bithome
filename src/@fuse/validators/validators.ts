import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export class FuseValidators
{
    /**
     * Check for empty (optional fields) values
     *
     * @param value
     */
    static isEmptyInputValue(value: any): boolean
    {
        return value == null || value.length === 0;
    }

    /**
     * Must match validator
     *
     * @param controlPath A dot-delimited string values that define the path to the control.
     * @param matchingControlPath A dot-delimited string values that define the path to the matching control.
     */
    static mustMatch(controlPath: string, matchingControlPath: string): ValidatorFn
    {
        return (formGroup: AbstractControl): ValidationErrors | null => {

            // Get the control and matching control
            const control = formGroup.get(controlPath);
            const matchingControl = formGroup.get(matchingControlPath);

            // Return if control or matching control doesn't exist
            if ( !control || !matchingControl )
            {
                return null;
            }

            // Delete the mustMatch error to reset the error on the matching control
            if ( matchingControl.hasError('mustMatch') )
            {
                delete matchingControl.errors.mustMatch;
                matchingControl.updateValueAndValidity();
            }

            // Don't validate empty values on the matching control
            // Don't validate if values are matching
            if ( this.isEmptyInputValue(matchingControl.value) || control.value === matchingControl.value )
            {
                return null;
            }

            // Prepare the validation errors
            const errors = {mustMatch: true};

            // Set the validation error on the matching control
            matchingControl.setErrors(errors);

            // Return the errors
            return errors;
        };
    }

    /**
     * Number validator with min, max
     *
     * @param control
     * @param params: {min: number, max: number}
     */
    static number(params: any = {}): ValidatorFn
    {
        return (control: AbstractControl): ValidationErrors | null => {
            let val: number = control.value;

            if (this.isEmptyInputValue(val)) {
                return null;
            } else if (!/^\d*[.]?\d{0,4}$/.test(val.toString()) || val <= 0) {
                return {"number": true};
            } else if (!isNaN(params.min) && !isNaN(params.max)) {
                return val < params.min || val > params.max ? {"numberMinMax": true} : null;
            } else if (!isNaN(params.min)) {
                return val < params.min ? {"numberMin": true} : null;
            } else if (!isNaN(params.max)) {
                return val > params.max ? {"numberMax": true} : null;
            } else {
                return null;
            }
        };
    }
}
