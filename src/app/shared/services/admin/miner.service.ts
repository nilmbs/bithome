import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';

import { HttpService } from 'app/shared/services/http.service';
import { User } from 'app/core/user/user.types';

@Injectable({
    providedIn: 'root'
})

export class AdminMinerService
{

    /**
     * Constructor
     */
    constructor(
        private _http: HttpClient,
        private _httpClient: HttpService
    ) {
    }

    /**
     * List all miners
     *
     */
    getList(): Observable<any>
    {
        return this._http.get<User[]>('api/common/miners').pipe(
            map((users: User[]) => users),
            catchError(this.handleError)
        );
    }

    /**
     * List all PCs of a miner
     *
     * @param email
     * @param token
     */
    getListPCs(email: string, token: string): Observable<any>
    {
        return this._httpClient.get(`mine/listpcs/${email}/${token}`).pipe(
            map(response => response),
            catchError(this.handleError)
        );
    }

    private handleError(error) {
        let errorMessage = 'Something went wrong, please try again.';
        if (error.error && error.error.hasOwnProperty('message')) {
            errorMessage = error.error.message;
        } else if (error.status != 404 && error.message) {
            errorMessage = error.message;
        }

        return throwError(errorMessage);
    }
}
