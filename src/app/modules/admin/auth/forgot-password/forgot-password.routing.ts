import { Route } from '@angular/router';
import { AdminForgotPasswordComponent } from './forgot-password.component';

export const adminForgotPasswordRoutes: Route[] = [
    {
        path     : '',
        component: AdminForgotPasswordComponent
    }
];
