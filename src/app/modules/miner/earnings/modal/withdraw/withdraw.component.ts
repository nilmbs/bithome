import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { FuseValidators } from '@fuse/validators';

import { FuseAlertType } from '@fuse/components/alert';
import { WalletService } from 'app/shared/services/wallet.service';
import { MinerService } from 'app/shared/services/miner.service';

@Component({
    selector: 'withdraw-earning-modal',
    templateUrl: './withdraw.component.html',
    styleUrls: ['./withdraw.component.scss']
})

export class WithdrawEarningModalComponent implements OnInit, OnDestroy {
    @ViewChild("withdrawModal", { static: false }) withdrawModal: TemplateRef<any>;

    public closeResult: string;
    public modalOpen: boolean = false;
    showAlert: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    withdrawalForm: FormGroup;
    isNewWallet: boolean = false;
    isSuccess: boolean = false;
    walletAddrs: any[] = [];

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private _walletService: WalletService,
        private _minerService: MinerService
    ) { }

    ngOnInit(): void {
        this.withdrawalForm = this.formBuilder.group({
            amount: ['', [Validators.required, FuseValidators.number()]],
            wallet_id: [''],
            wallet_name: [''],
            wallet_addr: [''],
            remember: [false]
        });
        this._walletService.getAll().subscribe((resp) => this.walletAddrs = resp);
    }

    get currentBalance() {
        return this._minerService.balance;
    }

    newWallet(isnew: boolean = false) {
        if (isnew) {
            this.isNewWallet = true;
            this.withdrawalForm.get('wallet_id').setValidators(null);
            this.withdrawalForm.get('wallet_name').setValidators([Validators.required]);
            this.withdrawalForm.get('wallet_addr').setValidators([Validators.required]);
            this.withdrawalForm.get('wallet_id').updateValueAndValidity();
        } else {
            this.isNewWallet = false;
            this.withdrawalForm.get('wallet_id').setValidators([Validators.required]);
            this.withdrawalForm.get('wallet_name').setValidators(null);
            this.withdrawalForm.get('wallet_addr').setValidators(null);
            this.withdrawalForm.get('wallet_id').updateValueAndValidity();
        }
    }

    sendToWallet() {
        this.showAlert = false;
        if ( this.withdrawalForm.invalid ) {
            return;
        }

        this.withdrawalForm.disable();
        var formdata = this.withdrawalForm.value;

        this._walletService.withdrawAmount(formdata)
            .pipe(
                finalize(() => {
                    this.withdrawalForm.enable();
                    this.showAlert = true;
                })
            )
            .subscribe(
                (resp) => {
                    this.isSuccess = true;
                    if (this.isNewWallet)
                        this._walletService.getAll().subscribe((resp) => this.walletAddrs = resp);
                },
                (error) => {
                    this.alert = {
                        type   : 'error',
                        message: error
                    };
                }
            );
    }

    openModal() {
        this.isSuccess = false;
        this.showAlert = false;
        this.withdrawalForm.reset({wallet_id: ''});
        this.newWallet();
        this.modalOpen = true;

        if (isPlatformBrowser(this.platformId)) {
            this.modalService.open(this.withdrawModal, { 
                size: 'lg',
                ariaLabelledBy: 'withdrawEarning-Modal',
                centered: true,
                windowClass: 'modal fade withdraw-earning' 
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if (this.modalOpen) {
            this.modalService.dismissAll();
        }
    }

    close() {
        this.modalService.dismissAll();
    }
}
