import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { bithomeCoinRoutes } from './bithome-coin.routing';
import { SharedModule } from 'app/shared/shared.module';
import { BithomeCoinComponent } from './bithome-coin.component';

@NgModule({
    declarations: [
        BithomeCoinComponent
    ],
    imports     : [
        RouterModule.forChild(bithomeCoinRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        FuseAlertModule,
        MatProgressSpinnerModule,
        SharedModule
    ]
})
export class BithomeCoinModule
{
}
