import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'add-pc-modal',
  templateUrl: './add-pc.component.html',
  styleUrls: ['./add-pc.component.scss']
})
export class AddPcModalComponent implements OnInit, OnDestroy {
  
  @Input() link: any = 'https://www.youtube.com/embed/dlxM8K0zbLw';
  @ViewChild("addPcModal", { static: false }) AddPcModal: TemplateRef<any>;

  public closeResult: string;
  public modalOpen: boolean = false;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private modalService: NgbModal,
    private _router: Router
  ) {
  }

  ngOnInit(): void {
  }

  openModal() {
    this.modalOpen = true;
    if (isPlatformBrowser(this.platformId)) { // For SSR 
      this.modalService.open(this.AddPcModal, { 
        size: 'lg',
        ariaLabelledBy: 'AddPcModal-Modal',
        centered: true,
        windowClass: 'modal fade addpc-modal' 
      }).result.then((result) => {
        `Result ${result}`
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnDestroy() {
    if (this.modalOpen) {
      this.modalOpen = false;
      this.modalService.dismissAll();
    }
  }

  goToPc() {
    this.modalOpen = false;
    this.modalService.dismissAll();
    this._router.navigate(['pcs']);
  }

  downloadPc() {
    this.modalOpen = false;
    this.modalService.dismissAll();
  }
}
