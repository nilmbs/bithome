import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SharedModule } from 'app/shared/shared.module';
import { pagesRoutes } from './pages-routing.module';
import { Error404Component } from './error/error-404/default.component';
import { Error500Component } from './error/error-500/default.component';
import { ComingSoonComponent } from './coming-soon/default.component';
import { TermsConditionComponent } from './terms-condition/default.component';
import { PrivacyPolicyComponent } from './privacy-policy/default.component';
import { HelpSupportComponent } from './help-support/default.component';

@NgModule({
  declarations: [
    Error404Component,
    Error500Component,
    ComingSoonComponent,
    TermsConditionComponent,
    PrivacyPolicyComponent,
    HelpSupportComponent
  ],
  imports: [
    RouterModule.forChild(pagesRoutes),
    SharedModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    FuseAlertModule,
    MatProgressSpinnerModule,
  ]
})
export class PagesModule { }
