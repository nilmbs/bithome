import { Component, OnInit, OnDestroy, ViewChild,
    TemplateRef, PLATFORM_ID, Inject, Input, Output, EventEmitter } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { FuseAlertType } from '@fuse/components/alert';

import { ToasterService } from 'app/shared/services/toaster.service';
import { Device } from 'app/shared/classes/device';

@Component({
    selector: 'pc-scheduling-modal',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})
export class PcschedulingModalComponent implements OnInit, OnDestroy {
    @ViewChild("pcSchedulingModal", { static: false }) addEditWalletModal: TemplateRef<any>;

    public closeResult: string;
    public modalOpen: boolean = false;
    showAlert: boolean = false;
    settingsForm: FormGroup;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    device: Device;

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private toasterService:ToasterService
    ) { }

    ngOnInit(): void {
        this.settingsForm = this.formBuilder.group({
            repeat: ['', [Validators.required]],
            repeat_days: this.formBuilder.array([], [Validators.required]),
        });
    }

    onChange(e: any) {
        const day = e.target.value;
        const daysArray: FormArray = this.settingsForm.get('repeat_days') as FormArray;
        if (e.target.checked) {
            if (daysArray.controls.findIndex(x => x.value == day) == -1) {
                daysArray.push(new FormControl(day));
            }
        } else {
            let index = daysArray.controls.findIndex(x => x.value == day)
            daysArray.removeAt(index);
        }
        daysArray.markAsTouched();
    }

    saveSettings() {console.log(this.settingsForm)
        if ( this.settingsForm.invalid ) {
            return;
        }

        this.settingsForm.disable();
        this.showAlert = false;
        var data = this.settingsForm.getRawValue();
        setTimeout(()=> {this.settingsForm.enable();this.modalService.dismissAll();}, 5000);
    }

    openModal(pc: Device) {
        this.showAlert = false;
        this.settingsForm.reset({'repeat': 'custom'});
        this.device = pc;

        this.modalOpen = true;
        if (isPlatformBrowser(this.platformId)) {
            this.modalService.open(this.addEditWalletModal, { 
                size: 'lg',
                ariaLabelledBy: 'pcScheduling-Modal',
                centered: true,
                windowClass: 'modal fade pc-scheduling' 
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if(this.modalOpen){
            this.modalService.dismissAll();
        }
    }
}
