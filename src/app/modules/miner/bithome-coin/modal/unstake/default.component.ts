import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FuseValidators } from '@fuse/validators';
import { FuseAlertType } from '@fuse/components/alert';

import { CommonUtils } from 'app/shared/utils/common.utils';
import { BithomeCoinService } from 'app/shared/services/bithome-coin.service';

@Component({
    selector: 'bitcoin-unstake-modal',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})

export class UnstakeModalComponent implements OnInit, OnDestroy {
    @ViewChild("unstakeModal", { static: false }) unstakeModal: TemplateRef<any>;

    public closeResult: string;
    public modalOpen: boolean = false;
    isConfirmed: boolean = false;
    showAlert: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    unstakedForm: FormGroup;

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private _bithomeCoinService: BithomeCoinService
    ) { }

    ngOnInit(): void {
        this.unstakedForm = this.formBuilder.group({
            amount: ['', [Validators.required, FuseValidators.number()]],
            stkpercent: [0]
        });
    }

    get stakeBalance(): number {
        return this.alldata.staked;
    }

    get alldata() {
        return this._bithomeCoinService.alldata;
    }

    changeUnstakeAmount(evt: any) {
        this.unstakedForm.get('stkpercent').setValue(evt.value);
        let amount = CommonUtils.percentageAmount(this.stakeBalance, evt.value);
        this.unstakedForm.get('amount').setValue(amount);
    }

    formatSliderLabel(value: number) {
        if (value == 100) return 'Max';
        return value+'%';
    }

    sliderValue(evt: any) {
        let amount = CommonUtils.percentageAmount(this.stakeBalance, evt.value);
        this.unstakedForm.get('amount').setValue(amount);
    }

    get unstakePercentText() {
        let ustkprcnt = this.unstakedForm.value.stkpercent;
        if (ustkprcnt == 100) return 'Max';
        return ustkprcnt+'%';
    }

    get unstakePercent() {
        return this.unstakedForm.value.stkpercent;
    }

    onChangeAmount(e) {
        let value = e.target.value;
        let percent = CommonUtils.calculatePercentage(this.stakeBalance, value);
        if (percent % 25 == 0) this.unstakedForm.get('stkpercent').setValue(percent);
        else this.unstakedForm.get('stkpercent').setValue(0);
    }

    confirmUnstake() {
        this.showAlert = false;
        if ( this.unstakedForm.invalid ) {
            return;
        }

        this.unstakedForm.disable();
        let formdata = this.unstakedForm.value;

        this._bithomeCoinService.updateData(formdata)
            .subscribe(
                (resp) => {
                    this.isConfirmed = true;
                    this.unstakedForm.reset();
                    this.unstakedForm.enable();
                    let balanceCurr = +this.alldata.balance + (+formdata.amount);
                    let stakedtotal = +this.alldata.total_staked - (+formdata.amount);
                    let currstaked = +this.alldata.staked - (+formdata.amount);
                    this._bithomeCoinService.alldata = {balance: balanceCurr, staked: currstaked, total_staked: stakedtotal};
                },
                (error) => {
                    this.showAlert = true;
                    this.unstakedForm.enable();
                    this.alert = {
                        type   : 'error',
                        message: error
                    };
                }
            );
    }

    cancel() {
        this.modalService.dismissAll();
    }

    openModal() {
        this.isConfirmed = false;
        this.showAlert = false;
        let amount = CommonUtils.percentageAmount(this.stakeBalance, 100);
        this.unstakedForm.reset({amount: amount, stkpercent: 100});
        this.modalOpen = true;
        if (isPlatformBrowser(this.platformId)) { // For SSR 
            this.modalService.open(this.unstakeModal, { 
                size: 'lg',
                ariaLabelledBy: 'bitcoinUnstake-Modal',
                centered: true,
                windowClass: 'modal fade bitcoin-unstake-wallet'
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if (this.modalOpen) {
            this.modalService.dismissAll();
        }
    }
}
