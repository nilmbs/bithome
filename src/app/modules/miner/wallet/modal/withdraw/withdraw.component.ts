import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { FuseValidators } from '@fuse/validators';

import { FuseAlertType } from '@fuse/components/alert';
import { WalletService } from 'app/shared/services/wallet.service';
import { MinerService } from 'app/shared/services/miner.service';

@Component({
    selector: 'withdraw-wallet-modal',
    templateUrl: './withdraw.component.html',
    styleUrls: ['./withdraw.component.scss']
})

export class WithdrawWalletModalComponent implements OnInit, OnDestroy {
    @ViewChild("withdrawModal", { static: false }) withdrawModal: TemplateRef<any>;

    public closeResult: string;
    showAlert: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    currencyControl = new FormControl('USDC', Validators.required);
    withdrawalForm: FormGroup;
    isNewWallet: boolean = false;
    isSuccess: boolean = false;
    walletAddrs: any[] = [];
    step: number = 1;

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private _walletService: WalletService,
        private _minerService: MinerService
    ) { }

    ngOnInit(): void {
        this.withdrawalForm = this.formBuilder.group({
            amount: ['', [Validators.required, FuseValidators.number()]]
        });
        this._walletService.getAll().subscribe((resp) => this.walletAddrs = resp);
    }

    get currentBalance() {
        return this._minerService.balance;
    }

    nextStep(step:number = 1) {
        if (this.step == 1) {
            if (this.currencyControl.value) this.step = step;
            else this.currencyControl.markAsTouched();
        } else
            this.step = step;
        if (step == 1) {
            this.resetData();
        }
    }

    resetData(isInit: boolean = false) {
        if (isInit) {
            this.step = 1;
            this.isNewWallet = false;
            this.withdrawalForm.reset({wallet_id: ''});
        }
        this.isSuccess = false;
        this.showAlert = false;
        if (this.isNewWallet) {
            this.withdrawalForm.removeControl('wallet_id');
            this.withdrawalForm.addControl('wallet_name', this.formBuilder.control('', [Validators.required]));
            this.withdrawalForm.addControl('wallet_addr', this.formBuilder.control('', [Validators.required]));
            this.withdrawalForm.addControl('remember', this.formBuilder.control(false, [Validators.required]));
        } else {
            this.withdrawalForm.addControl('wallet_id', this.formBuilder.control('', [Validators.required]));
            this.withdrawalForm.removeControl('wallet_name');
            this.withdrawalForm.removeControl('wallet_addr');
            this.withdrawalForm.removeControl('remember');
        }
    }

    newWallet(isnew: boolean = false, form: NgForm = null) {
        this.isNewWallet = isnew;
        if (form) form.resetForm({amount: form.value.amount});
        this.resetData();
    }

    sendToWallet() {
        this.showAlert = false;
        if ( this.withdrawalForm.invalid ) {
            return;
        }

        this.withdrawalForm.disable();
        var formdata = this.withdrawalForm.value;

        this._walletService.withdrawAmount(formdata)
            .pipe(
                finalize(() => {
                    this.withdrawalForm.enable();
                    this.showAlert = true;
                })
            )
            .subscribe(
                (resp) => {
                    this.isSuccess = true;
                    if (this.isNewWallet)
                        this._walletService.getAll().subscribe((resp) => this.walletAddrs = resp);
                },
                (error) => {
                    this.alert = {
                        type   : 'error',
                        message: error
                    };
                }
            );
    }

    openModal() {
        this.resetData(true);

        if (isPlatformBrowser(this.platformId)) {
            this.modalService.open(this.withdrawModal, { 
                size: 'lg',
                ariaLabelledBy: 'withdrawWallet-Modal',
                centered: true,
                windowClass: 'modal fade withdraw-wallet' 
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        this.modalService.dismissAll();
    }

    close() {
        this.modalService.dismissAll();
    }
}
