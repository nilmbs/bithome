import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { HttpService } from 'app/shared/services/http.service';
import { LoggedUser, LoggedUserDefault } from 'app/shared/classes/loggeduser';
import { UserService } from 'app/core/user/user.service';
import { MinerService } from 'app/shared/services/miner.service';

export const TOKEN_KEY = 'auth-token';
export const USER_KEY  = 'auth-user';

@Injectable()
export class AuthService
{
    private _authenticated: boolean = false;
    private currentUser: BehaviorSubject<LoggedUser> = new BehaviorSubject<LoggedUser>(LoggedUserDefault);

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpService,
        private _userService: UserService,
        private _minerService: MinerService
    ) {
        if (localStorage.getItem(USER_KEY) && localStorage.getItem(TOKEN_KEY)) {
            let user = JSON.parse(localStorage.getItem(USER_KEY));
            if (user && user.id) {
                this._authenticated = true;
                this.currentUser.next(user);
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for access token
     */
    set accessToken(token: string)
    {
        localStorage.setItem(TOKEN_KEY, token);
    }

    get accessToken(): string
    {
        return localStorage.getItem(TOKEN_KEY) ?? '';
    }

    set user(user: LoggedUser)
    {
        localStorage.setItem(USER_KEY, JSON.stringify(user));
        this.currentUser.next(user);
    }

    get user(): LoggedUser
    {
        return this.currentUser.value;
    }

    get userId(): any {
        if (this.user) {
            return this.user.id;
        }

        return '';
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    isLoggedIn() {
        return this._authenticated;
    }

    loginAfterRegister(resp: any) {
        this.resetCredentials();
        this.accessToken = resp.token;
        let tokendecode = AuthUtils._decodeToken(this.accessToken);

        this._authenticated = true;

        let user = {id: tokendecode._id, name: 'Dave Admin', email: tokendecode.email, type: 'miner'};
        //delete user.token;
        this.user = user;
    }

    /**
     * Forgot password
     *
     * @param email
     */
    forgotPassword(email: string): Observable<any>
    {
        return this._httpClient.post('auth/forgot-password', {email: email }).pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    /**
     * Reset password
     *
     * @param email
     * @param token
     * @param password
     */
    resetPassword(email: string, token: string, password: string): Observable<any>
    {
        return this._httpClient.post(`auth/reset-password/${email}/${token}`, {password: password});
    }

    /**
     * Reset password link verify
     *
     * @param email
     * @param token
     */
    verifyResetPasswordLink(email: string, token: string): Observable<any>
    {
        return this._httpClient.get(`auth/reset-password/${email}/${token}`);
    }

    /**
     * Sign in
     *
     * @param postdata
     */
    signIn(postdata: { email: string; password: string, rememberMe: boolean }): Observable<any>
    {
        // Throw error, if the user is already logged in
        if ( this._authenticated ) {
            return throwError('User is already logged in.');
        }
        let credentials = {
            email: postdata.email,
            password: postdata.password
        }

        return this._httpClient.post('auth/login', credentials).pipe(
            map((response: any) => {
                this.resetCredentials();
                this.accessToken = response.token;
                let tokendecode = AuthUtils._decodeToken(this.accessToken);

                this._authenticated = true;

                let user = {id: tokendecode._id, name: 'Dave Admin', email: tokendecode.email, type: 'miner'};
                //delete user.token;
                this.user = user;

                return response;
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Admin Sign in
     *
     * @param postdata
     */
    adminSignIn(postdata: { email: string; password: string, rememberMe: boolean }): Observable<any>
    {
        // Throw error, if the user is already logged in
        if ( this._authenticated ) {
            return throwError('User is already logged in.');
        }
        let credentials = {
            email: postdata.email,
            password: postdata.password
        }

        return this._httpClient.post('auth/login', credentials).pipe(
            map((response: any) => {
                this.resetCredentials();
                this.accessToken = response.token;
                let tokendecode = AuthUtils._decodeToken(this.accessToken);

                this._authenticated = true;

                let user = {id: tokendecode._id, name: 'Dave Admin', email: tokendecode.email, type: 'admin'};
                //delete user.token;
                this.user = user;

                return response;
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Sign in using the access token
     */
    signInUsingToken(): Observable<any>
    {
        // Renew token
        return this._httpClient.post('auth/refresh-access-token', {
            token: this.accessToken
        }).pipe(
            catchError(() =>
                // Return false
                of(false)
            ),
            switchMap((response: any) => {
                this.accessToken = response.token;
                this._authenticated = true;
                return of(true);
            })
        );
    }

    private resetCredentials() {
        this._authenticated = false;
        localStorage.removeItem(USER_KEY);
        localStorage.removeItem(TOKEN_KEY);
        this.currentUser.next(LoggedUserDefault);
        this._userService.user = {id: '', name: '', email: ''};
        this._minerService.miner = null;
    }

    /**
     * Sign out
     */
    signOut(): boolean
    {
        this.resetCredentials();

        return true;
    }

    /**
     * Sign up
     *
     * @param user
     */
    signUp(userdata): Observable<any>
    {
        return this._httpClient.post('auth/register', userdata).pipe(
            map((response: Response) => response),
            catchError(this.handleError)
        );
    }

    /**
     * Unlock session
     *
     * @param credentials
     */
    unlockSession(credentials: { email: string; password: string }): Observable<any>
    {
        return this._httpClient.post('api/auth/unlock-session', credentials);
    }

    /**
     * Check the authentication status
     */
    check(): Observable<boolean>
    {
        if ( this._authenticated ) {
            return of(true);
        }

        if ( !this.accessToken ) {
            return of(false);
        }

        if ( AuthUtils.isTokenExpired(this.accessToken) ) {
            return of(false);
        }

        return this.signInUsingToken();
    }

    private handleError(error) {
        let errorMessage = 'Something went wrong, please try again.';
        if (error.error && error.error.hasOwnProperty('message')) {
            errorMessage = error.error.message;
        } else if (error.status != 404 && error.message) {
            errorMessage = error.message;
        }

        return throwError(errorMessage);
    }
}
