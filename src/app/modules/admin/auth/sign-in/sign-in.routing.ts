import { Route } from '@angular/router';
import { AdminSignInComponent } from './sign-in.component';

export const adminSignInRoutes: Route[] = [
    {
        path     : '',
        component: AdminSignInComponent
    }
];
