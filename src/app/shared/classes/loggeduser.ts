export interface LoggedUser {
	id: string;
	token?: string;
	name: string;
    email: string;
    type: string;
}

export let LoggedUserDefault = {
	id: '',
	token: '',
	name: '',
    email: '',
    type: ''
}