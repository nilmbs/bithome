import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseConfigService, FuseAdminConfigService } from '@fuse/services/config';
import { AppConfig, Scheme } from 'app/core/config/app.config';
import { UserService } from 'app/core/user/user.service';

@Component({
    selector     : 'setting-change-scheme',
    templateUrl  : './scheme.component.html',
    styleUrls    : ['./scheme.component.scss']
})

export class ChangeSchemaComponent implements OnInit, OnDestroy
{
    config: AppConfig;
    scheme: 'dark' | 'light';
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _fuseConfigService: FuseConfigService,
        private _fuseAdminConfigService: FuseAdminConfigService,
        private _userService: UserService
    ) {
    }

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Subscribe to config changes
        if (this.isAdmin) {
            this._fuseAdminConfigService.config$
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe((config: AppConfig) => {
                    this.config = config;
                });
        } else {
            this._fuseConfigService.config$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config: AppConfig) => {

                // Store the config
                this.config = config;
            });
        }
    }

    /**
     * Set the scheme on the config
     *
     * @param evt
     */
    setScheme(evt: any): void
    {
        let scheme: Scheme = 'light';
        if (evt.target.checked) scheme = 'dark';
        if (this.isAdmin)
            this._fuseAdminConfigService.config = {scheme};
        else
            this._fuseConfigService.config = {scheme};
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    get isAdmin() {
        return this._router.url.includes('/admin');
    }
}
