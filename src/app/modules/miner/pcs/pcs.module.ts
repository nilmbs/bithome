import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@fuse/components/alert';

import { pcsRoutes } from './pcs.routing';
import { SharedModule } from 'app/shared/shared.module';
import { PcsComponent } from './pcs.component';
import { PcschedulingModalComponent } from './modal/scheduling/default.component';

@NgModule({
    declarations: [
        PcsComponent,
        PcschedulingModalComponent
    ],
    imports     : [
        RouterModule.forChild(pcsRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        FuseAlertModule,
        MatCheckboxModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        SharedModule
    ]
})
export class PcsModule
{
}
