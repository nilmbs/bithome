import { Inject, Pipe, PipeTransform, DEFAULT_CURRENCY_CODE } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { isObject } from 'rxjs/util/isObject';
import { getCurrencySymbol, formatCurrency } from '@angular/common';
import { CommonUtils } from 'app/shared/utils/common.utils';

@Pipe({
  	name: 'currency0'
})

export class Currency0Pipe extends CurrencyPipe implements PipeTransform {
	constructor(@Inject(DEFAULT_CURRENCY_CODE) private _defaultCurrCode: string)
	{
		super('en', _defaultCurrCode);
	}

	transform(value: number = 0, currency: string = this._defaultCurrCode, symbol: string | boolean = 'symbol', digits: string = '1.3-3'): any
	{
		//if (currency == '') currency = this._defaultCurrCode;
    	let showsym: any = 'wide';
    	if (symbol == 'symbol-narrow') showsym = 'narrow';
    	let currency_symbol = getCurrencySymbol(currency, showsym);
    	if (symbol == 'code') currency_symbol = currency;

    	let mdigit: number = parseInt(digits.split('.')[1].slice(0, 1));

    	return (symbol?currency_symbol:'') + CommonUtils.toFixedNumber(value, mdigit);
  	}
}

@Pipe({
  	name: 'currency1'
})

export class Currency1Pipe extends CurrencyPipe implements PipeTransform {
	constructor(@Inject(DEFAULT_CURRENCY_CODE) private _defaultCurrCode: string)
	{
		super('en', _defaultCurrCode);
	}

	transform(value: number = 0, currency: string = this._defaultCurrCode, symbol: string | boolean = 'symbol', digits: string = '1.3-3'): any
	{
		//if (currency == '') currency = this._defaultCurrCode;
    	let showsym: any = 'wide';
    	if (symbol == 'symbol-narrow') showsym = 'narrow';
    	let currency_symbol = getCurrencySymbol(currency, showsym);
    	if (symbol == 'code') currency_symbol = currency;

    	let mdigit: number = parseInt(digits.split('.')[1].slice(0, 1));

    	return (symbol?'<sub>' + currency_symbol + '</sub>':'') + CommonUtils.toFixedNumber(value, mdigit);
  	}
}

@Pipe({
  	name: 'currency2'
})

export class Currency2Pipe extends CurrencyPipe implements PipeTransform {
	constructor(@Inject(DEFAULT_CURRENCY_CODE) private _defaultCurrCode: string)
	{
		super('en', _defaultCurrCode);
	}

	transform(value: number = 0, currency: string = this._defaultCurrCode, symbol: string | boolean = 'symbol', digits: string = '1.3-3'): any
	{
		if (currency == '' && symbol != false) currency = this._defaultCurrCode;
    	let showsym: any = 'wide';
    	if (symbol == 'symbol-narrow') showsym = 'narrow';
    	let currency_symbol = getCurrencySymbol(currency, showsym);
    	if (symbol == 'code') currency_symbol = currency;

    	let mdigit: number = parseInt(digits.split('.')[1].slice(0, 1));
		//formatCurrency(value, 'en', '', currency, digits)
    	return CommonUtils.toFixedNumber(value, 3) + (symbol?'<sub>' + currency_symbol + '</sub>':'');
  	}
}
