import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, timer } from 'rxjs';
import { finalize, takeUntil, takeWhile, tap } from 'rxjs/operators';
import { FuseAdminConfigService } from '@fuse/services/config';
import { AppConfig } from 'app/core/config/app.config';
import { AuthService } from 'app/core/auth/auth.service';
import { IdleService } from 'app/shared/services/idle.service';

@Component({
    selector     : 'auth-admin-sign-out',
    templateUrl  : './sign-out.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AdminSignOutComponent implements OnInit, OnDestroy
{
    countdown: number = 5;
    countdownMapping: any = {
        '=1'   : '# second',
        'other': '# seconds'
    };
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _fuseAdminConfigService: FuseAdminConfigService,
        private _idleService: IdleService,
        private _authService: AuthService
    ) {
    }

    get config(): AppConfig {
        return this._fuseAdminConfigService.config;
    }

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Sign out
        this._authService.signOut();
        this._idleService.isSetLastAtvtm = true;

        // Redirect after the countdown
        timer(1000, 1000)
            .pipe(
                finalize(() => {
                    this._router.navigate(['admin/sign-in']);
                }),
                takeWhile(() => this.countdown > 0),
                takeUntil(this._unsubscribeAll),
                tap(() => this.countdown--)
            )
            .subscribe();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
