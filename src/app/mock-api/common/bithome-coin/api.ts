import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash-es';
import { FuseMockApiService } from '@fuse/lib/mock-api';
import { data as bthData, earnings as earningsData, profits as profitsData } from './data';

@Injectable({
    providedIn: 'root'
})
export class BithomeCoinMockApi
{
    private _data: any = bthData;
    private _earnings: any = earningsData;
    private _profits: any = profitsData;

    /**
     * Constructor
     */
    constructor(private _fuseMockApiService: FuseMockApiService)
    {
        // Register Mock API handlers
        this.registerHandlers();
    }

    /**
     * Register Mock API handlers
     */
    registerHandlers(): void
    {
        this._fuseMockApiService
            .onGet('api/bithome-coin/data')
            .reply(() => [200, cloneDeep(this._data)]);
        this._fuseMockApiService
            .onGet('api/bithome-coin/earning')
            .reply(() => [200, cloneDeep(this._earnings)]);
        this._fuseMockApiService
            .onGet('api/bithome-coin/profit')
            .reply(() => [200, cloneDeep(this._profits)]);
    }
}
