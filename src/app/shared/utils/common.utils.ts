

export class CommonUtils
{
    /**
     * Constructor
     */
    constructor() {
    }

    /**
     * Convert hash rate
     *
     * @param rate
     */
    static convertHashRate(rate: number): string
    {
        if (rate === 0) return '0 Hz';

        const k = 1000;
        const sizes = ['Hz', 'KHz', 'MHz', 'GHz', 'THz', 'PHz'];

        const i = Math.floor(Math.log(rate) / Math.log(k));

        return parseFloat((rate / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    /**
     * Get percentage amount 
     *
     * @param amount
     * @param percent
     */
    static percentageAmount(amount: number, percent: number): number
    {
        let pamt = (amount * percent) / 100;
        return parseFloat(this.toFixedNumber(pamt,3));
    }

    /**
     * Get percentage value of amount 
     *
     * @param amountm
     * @param amount
     */
    static calculatePercentage(amountm: number, amount: number): number
    {
        let percent = (amount * 100) / amountm;
        return parseFloat(percent.toFixed(2));
    }

    /**
     * Sort a array of objects by key
     *
     * @param items
     * @param field
     * @param sortby
     */
    static sortData(items: any[], field: string, sortby: string = 'asc'): any {
        if (sortby === 'a-z') {
            return items.sort((a, b) => {
                if (a[field].toUpperCase() < b[field].toUpperCase()) {
                    return -1;
                } else if (a[field].toUpperCase() > b[field].toUpperCase()) {
                    return 1;
                }
                return 0;
            })
        } else if (sortby === 'z-a') {
            return items.sort((a, b) => {
                if (a[field].toUpperCase() > b[field].toUpperCase()) {
                    return -1;
                } else if (a[field].toUpperCase() < b[field].toUpperCase()) {
                    return 1;
                }
                return 0;
            })
        } else if (sortby === 'asc') {
            return items.sort((a, b) => {
                if (a[field] < b[field]) {
                    return -1;
                } else if (a[field] > b[field]) {
                    return 1;
                }
                return 0;
            })
        } else if (sortby === 'desc') {
            return items.sort((a, b) => {
                if (a[field] > b[field]) {
                    return -1;
                } else if (a[field] < b[field]) {
                    return 1;
                }
                return 0;
            })
        } 
    }

    /**
     * Fix a number with decimals without round 
     *
     * @param value
     * @param decimalPlaces
     */
    static toFixedNumber(value: number, decimalPlaces: number = 2) {
        const valueString: string = value.toString();
        const pointIndex: number = valueString.indexOf('.');

        // Return the integer part if decimalPlaces is 0
        if (decimalPlaces === 0) {
            return valueString.substr(0, pointIndex);
        }

        // Return value with 0s appended after decimal if the price is an integer
        if (pointIndex === -1) {
            const padZeroString: string = '0'.repeat(decimalPlaces);

            return `${valueString}.${padZeroString}`;
        }

        // If numbers after decimal are less than decimalPlaces, append with 0s
        const padZeroLen: number = valueString.length - pointIndex - 1;
        if (padZeroLen > 0 && padZeroLen < decimalPlaces) {
            const padZeroString: string = '0'.repeat(decimalPlaces - padZeroLen);

            return `${valueString}${padZeroString}`;
        }

        return valueString.substr(0, pointIndex + decimalPlaces + 1);
    }
}
