import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector	  : 'app-page-privacy-policy',
  templateUrl : './default.component.html',
  styleUrls   : ['./default.component.scss']
})

export class PrivacyPolicyComponent implements OnInit {

  constructor(
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }
}
