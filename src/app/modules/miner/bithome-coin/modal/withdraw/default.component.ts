import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { BithomeCoinService } from 'app/shared/services/bithome-coin.service';

@Component({
    selector: 'bitcoin-withdraw-modal',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})
export class WithdrawModalComponent implements OnInit, OnDestroy {
    @ViewChild("withdrawModal", { static: false }) withdrawModal: TemplateRef<any>;

    public closeResult: string;
    public modalOpen: boolean = false;

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private _bithomeCoinService: BithomeCoinService
    ) { }

    ngOnInit(): void {
    }

    get balance(): number {
        return this._bithomeCoinService.balance;
    }

    confirm() {
        this.modalService.dismissAll();
    }

    cancel() {
        this.modalService.dismissAll();
    }

    openModal() {
        this.modalOpen = true;
        if (isPlatformBrowser(this.platformId)) { // For SSR 
            this.modalService.open(this.withdrawModal, { 
                size: 'lg',
                ariaLabelledBy: 'bitcoinWithdraw-Modal',
                centered: true,
                windowClass: 'modal fade bitcoin-withdraw-wallet'
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if (this.modalOpen) {
            this.modalService.dismissAll();
        }
    }
}
