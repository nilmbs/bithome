import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';
import { merge } from 'lodash-es';

import { HttpService } from 'app/shared/services/http.service';
import { BithomeCoinData, DefaultBthCoinData } from 'app/shared/classes/bithome-coindata';

@Injectable({
    providedIn: 'root'
})

export class BithomeCoinService
{
    private _alldata: BehaviorSubject<BithomeCoinData> = new BehaviorSubject<BithomeCoinData>(DefaultBthCoinData);

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpService,
        private _http: HttpClient
    ) {
    }

    set alldata(value: any)
    {
        const data = merge({}, this._alldata.getValue(), value);

        // Execute the observable
        this._alldata.next(data);
    }

    get alldata(): BithomeCoinData
    {
        return this._alldata.value;
    }

    get balance(): any
    {
        return this.alldata.balance;
    }

    /**
     * Get data (balance, staked, total_staked, apy)
     *
     */
    getData(): Observable<any>
    {
        return this._http.get('api/bithome-coin/data').pipe(
            map((response: any) => {
                const data = merge(this._alldata.getValue(), response);
                this._alldata.next(data);
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Get earnings (total, unstaked)
     *
     */
    getEarnings(): Observable<any>
    {
        return this._http.get('api/bithome-coin/earning').pipe(
            map((response: any) => {
                const data = merge(this._alldata.getValue(), response);
                this._alldata.next(data);
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Get profit (7days, lifetime)
     *
     */
    getProfits(): Observable<any>
    {
        return this._http.get('api/bithome-coin/profit').pipe(
            map((response: any) => {
                const data = merge(this._alldata.getValue(), response);
                this._alldata.next(data);
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Get recent activity
     *
     */
    getRecentActivity(): Observable<any>
    {
        return this._http.get('api/common/activities').pipe(
            map((response: any) => response.bithomecoin),
            catchError(this.handleError)
        );
    }

    /**
     * Update bithomecoin data
     *
     */
    updateData(postdata): Observable<any>
    {
        return of(true);
        return this._httpClient.post('bithome-coin/updatedata', postdata).pipe(
            map((response: any) => response),
            catchError(this.handleError)
        );
    }

    private handleError(error) {
        let errorMessage = 'Something went wrong, please try again.';
        if (error.error && error.error.hasOwnProperty('message')) {
            errorMessage = error.error.message;
        } else if (error.status != 404 && error.message) {
            errorMessage = error.message;
        }

        return throwError(errorMessage);
    }
}
