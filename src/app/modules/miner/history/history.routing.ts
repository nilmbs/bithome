import { Route} from '@angular/router';

import { HistoryComponent } from './history.component';

export const historyRoutes: Route[] = [
    {
        path     : '',
        component: HistoryComponent
    }
];

