import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FuseValidators } from '@fuse/validators';
import { FuseAlertType } from '@fuse/components/alert';

import { CommonUtils } from 'app/shared/utils/common.utils';
import { BithomeCoinService } from 'app/shared/services/bithome-coin.service';

@Component({
    selector: 'bitcoin-stake-modal',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})

export class StakeModalComponent implements OnInit, OnDestroy {
    @ViewChild("stakeModal", { static: false }) stakeModal: TemplateRef<any>;

    public closeResult: string;
    public modalOpen: boolean = false;
    isConfirmed: boolean = false;
    showAlert: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    stakedForm: FormGroup;

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private _bithomeCoinService: BithomeCoinService
    ) { }

    ngOnInit(): void {
        this.stakedForm = this.formBuilder.group({
            amount: ['', [Validators.required, FuseValidators.number()]],
            stkpercent: [0]
        });
    }

    get balance(): number {
        return this._bithomeCoinService.balance;
    }

    get alldata() {
        return this._bithomeCoinService.alldata;
    }

    changeStakeAmount(evt: any) {
        this.stakedForm.get('stkpercent').setValue(evt.value);
        let amount = CommonUtils.percentageAmount(this.balance, evt.value);
        this.stakedForm.get('amount').setValue(amount);
    }

    formatSliderLabel(value: number) {
        if (value == 100) return 'Max';
        return value+'%';
    }

    sliderValue(evt: any) {
        let amount = CommonUtils.percentageAmount(this.balance, evt.value);
        this.stakedForm.get('amount').setValue(amount);
    }

    get stakePercentText() {
        let stkprcnt = this.stakedForm.value.stkpercent;
        if (stkprcnt == 100) return 'Max';
        return stkprcnt+'%';
    }

    get stakePercent() {
        return this.stakedForm.value.stkpercent;
    }

    onChangeAmount(e) {
        let value = e.target.value;
        let percent = CommonUtils.calculatePercentage(this.balance, value);
        if (percent % 25 == 0) this.stakedForm.get('stkpercent').setValue(percent);
        else this.stakedForm.get('stkpercent').setValue(0);
    }

    confirmStake() {
        this.showAlert = false;
        if ( this.stakedForm.invalid ) {
            return;
        }

        this.stakedForm.disable();
        let formdata = this.stakedForm.value;

        this._bithomeCoinService.updateData(formdata)
            .subscribe(
                (resp) => {
                    this.isConfirmed = true;
                    this.stakedForm.reset();
                    this.stakedForm.enable();
                    let balanceCurr = +this.balance - (+formdata.amount);
                    let stakedtotal = +this.alldata.total_staked + (+formdata.amount);
                    let currstaked = +this.alldata.staked + (+formdata.amount);
                    this._bithomeCoinService.alldata = {balance: balanceCurr, staked: currstaked, total_staked: stakedtotal};
                },
                (error) => {
                    this.showAlert = true;
                    this.stakedForm.enable();
                    this.alert = {
                        type   : 'error',
                        message: error
                    };
                }
            );
    }

    cancel() {
        this.modalService.dismissAll();
    }

    openModal() {
        this.isConfirmed = false;
        this.showAlert = false;
        let amount = CommonUtils.percentageAmount(this.balance, 100);
        this.stakedForm.reset({amount: amount, stkpercent: 100});
        this.modalOpen = true;
        if (isPlatformBrowser(this.platformId)) { // For SSR 
            this.modalService.open(this.stakeModal, { 
                size: 'lg',
                ariaLabelledBy: 'bitcoinStake-Modal',
                centered: true,
                windowClass: 'modal fade bitcoin-stake-wallet'
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if (this.modalOpen) {
            this.modalService.dismissAll();
        }
    }
}
