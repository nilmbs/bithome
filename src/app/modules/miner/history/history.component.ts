import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import * as moment from 'moment';
import { MatButtonToggleChange } from '@angular/material/button-toggle';

import { Currency0Pipe } from 'app/shared/pipes/currencies.pipe';
import { MinerService } from 'app/shared/services/miner.service';
import { CommonUtils } from 'app/shared/utils/common.utils';

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.scss']
})

export class HistoryComponent implements OnInit {
    @ViewChild("avgProfitChart") avgProfitChart: ChartComponent;
    avgProfitChartOpts: any = {};
    pastDaysEarning: number = 0;
    allTimeEarning: number = 0;
    avgProfitType: 'hourly' | 'daily' | 'weekly' = 'hourly';
    chartDataLoader: boolean = true;
    chartData: any = {
        hourly: {labels: [], values: []},
        daily: {labels: [], values: []},
        weekly: {labels: [], values: []}
    };

    constructor(
        private currencyPipe : Currency0Pipe,
        private _minerService: MinerService
    ) {}

    ngOnInit(): void {
        this._minerService.getTotalEarningHistory().subscribe((resp) => {
            this.pastDaysEarning = resp;
            this.allTimeEarning = resp;
        });
        this._minerService.getAvgProfitHistory().subscribe((resp) => {
            this.prepareProfitData(resp);
        });
        this._prepareChartData();
    }

    prepareProfitData(resp: any) {//console.log(resp)
        let hourlyearn = resp[0].data;
        let dailyearn = resp[1].data;
        let weeklyearn = resp[2].data;
        // let hourlyearnn = resp[3].data;
        // const hourlyEarned = hourlyearnn.reduce((ndata, cur) => {
        //     const item = ndata.length > 0 && ndata.find(({
        //         time
        //     }) => time === cur.time);
        //     if (item) item.earning += cur.earning;
        //     else ndata.push({time: cur.time, earning:cur.earning, hour: moment.unix(cur.time).hour()});
        //     return ndata;
        // }, []).reduce((ndata, cur) => {
        //     const item = ndata.length > 0 && ndata.find(({
        //         hour
        //     }) => hour === cur.hour);
        //     if (item) item.earning += cur.earning;
        //     else ndata.push({time: cur.time, earning:cur.earning, hour: moment.unix(cur.time).hour()});
        //     return ndata;
        // }, []);
        hourlyearn = CommonUtils.sortData(hourlyearn, 'startDate', 'desc');
        //hourlyearnn = CommonUtils.sortData(hourlyearnn, 'time', 'desc');
        dailyearn = CommonUtils.sortData(dailyearn, 'startDate', 'desc');
        weeklyearn = CommonUtils.sortData(weeklyearn, 'startDate', 'desc');
        // hourlyEarned.forEach((item, key) => {
        //     let mdt = moment.unix(item.time);
        //    // console.log(mdt.format('DD/MM/YYYY h:mm A'), mdt.hour());
        // });
        hourlyearn = hourlyearn.filter((item, key) => {            
            let mdt = moment.unix(item.endDate);
            if (moment(mdt.format('YYYY-MM-DD')).isBefore(moment().format('YYYY-MM-DD'))) return;
            item.hour = mdt.hour();
            return item;
        });
        dailyearn.forEach((item, key) => {
            let mdt = moment.unix(item.endDate);
            if (moment(mdt.format('YYYY-MM-DD')).isAfter(moment().format('YYYY-MM-DD'))) return;
            this.chartData.daily.labels.unshift(mdt.format('MM/DD'));
            this.chartData.daily.values.unshift(item.earning);
        });
        weeklyearn.forEach((item, key) => {
            let mdt = moment.unix(item.startDate);
            let mdt1 = moment.unix(item.endDate);
            if (moment(mdt1.format('YYYY-MM-DD')).isAfter(moment().format('YYYY-MM-DD'))) return;
            this.chartData.weekly.labels.unshift(mdt.format('MM/DD')+' - '+mdt1.format('MM/DD'));
            this.chartData.weekly.values.unshift(item.earning);
        });
        const currHour = moment().hour();
        let hourlyData = {labels: [], values: []};
        for(let h=0; h <= currHour; h++) {
            const ernitem = hourlyearn.find((item) => item.hour === h) || {earning: 0};
            let time = moment().startOf('day').add(moment.duration(h, 'hours')).format('h:mm A');
            hourlyData.labels.push(time);
            hourlyData.values.push(ernitem.earning);
        }
        this.chartData.hourly = hourlyData;
        this.avgProfitChartOpts.xaxis.categories = hourlyData.labels;
        this.avgProfitChartOpts.yaxis.axisBorder.show = hourlyData.values.length > 0 ? false : true;
        this.avgProfitChartOpts.series = [{
            name: 'Hourly Avg Profit',
            data: hourlyData.values
        }];
        this.avgProfitChart.updateOptions({
            xaxis: this.avgProfitChartOpts.xaxis,
            yaxis: this.avgProfitChartOpts.yaxis
        }, true);
        this.chartDataLoader = false;
    }

    changeAvgProfitType(change: MatButtonToggleChange) {
        this.avgProfitType = change.value;

        let values = this.chartData[this.avgProfitType].values;
        this.avgProfitChartOpts.xaxis.categories = this.chartData[this.avgProfitType].labels;
        let yaxisOpt = this.avgProfitChartOpts.yaxis;
        if (this.avgProfitType == 'hourly') delete yaxisOpt.max;
        else {
            let maxVal = Math.max(...values)
            yaxisOpt.max = this.roundUp(Math.max(...values));
        }
        yaxisOpt.axisBorder.show = values.length > 0 ? false : true;

        this.avgProfitChart.updateSeries([{
            data: values
        }])
        this.avgProfitChart.updateOptions({
                xaxis: this.avgProfitChartOpts.xaxis,
                yaxis: yaxisOpt
            }, true)

    }

    private roundUp(value: number)
    {
        if (value % 10 == 0) return value;
        return (10 - value % 10) + value;
    }

    private _prepareChartData() {
        const $this = this;

        this.avgProfitChartOpts = {
            chart     : {
                animations: {
                    speed           : 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor : 'inherit',
                width     : '100%',
                height    : '100%',
                type      : 'area',
                toolbar   : {
                    show: false
                },
                zoom      : {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            fill      : {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    gradientToColors: [ '#e7fdf9'],
                    shadeIntensity: 0.15,
                    type: 'vertical',
                    opacityFrom: .21,
                    opacityTo: .12,
                    stops: [0, 100]
                },
            },
            grid      : {
                show       : true,
                borderColor: '#E4E4E6',
                position   : 'back',
                xaxis      : {
                    lines: {
                        show: true
                    }
                },
                yaxis      : {
                    lines: {
                        show: false
                    }
                }
            },
            markers: {
                size: 7,
                colors: '#FFF',
                strokeColors: '#00D091',
                strokeOpacity: 1,
                hover: {
                    size: 7
                },
            },
            stroke    : {
                width: 2,
                curve: 'straight',
                colors: ['#62EED3']
            },
            tooltip   : {
                shared      : false,
                intersect   : true,
                theme       : 'dark',
                custom: function({series, seriesIndex, dataPointIndex, w}) {
                    let value = $this.currencyPipe.transform(parseFloat(series[seriesIndex][dataPointIndex]));
                    return `<div class="flex items-center flex-col custom-tooltip">
                                <div class="label">${$this.chartData[$this.avgProfitType].labels[dataPointIndex]}</div>
                                <div class="value">${value}</div>
                            </div>`
                }
            },
            series    : [],
            xaxis     : {
                    axisBorder: {
                        show: true,
                        color: '#CCC',
                    },
                    axisTicks : {
                        show: false
                    },
                    labels    : {
                        offsetY: 3,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                        },
                        formatter: function(value) {
                            let val = '';
                            if (!!value) {
                                val = value.replace(/\sAM/i, '').replace(/\sPM/i, '')
                            }
                            return val;
                        }
                    },
                    tickAmount: 8,
                    tooltip   : {
                        enabled: false
                    },
                    crosshairs: {
                        show: false,
                    },
                    categories: []

                /*daily: {
                    axisBorder: {
                        show: true,
                        color: '#CCC',
                    },
                    axisTicks : {
                        show: false
                    },
                    labels    : {
                        offsetY: 3,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                        }
                    },
                    tooltip   : {
                        enabled: false
                    },
                    crosshairs: {
                        show: false,
                    }
                },
                weekly: {
                    axisBorder: {
                        show: true,
                        color: '#CCC',
                    },
                    axisTicks : {
                        show: false
                    },
                    labels    : {
                        offsetY: 3,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                        }
                    },
                    tooltip   : {
                        enabled: false
                    },
                    crosshairs: {
                        show: false,
                    }
                },*/
            },
            yaxis     : {
                    axisTicks : {
                        show: false
                    },
                    axisBorder: {
                        show: true,
                        offsetX: 9,
                    },
                    min: 0,
                    tickAmount: 5,
                    labels    : {
                        offsetX: -9,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                            fontSize: '13px'
                        },
                        formatter: function(value) {
                            return $this.currencyPipe.transform(value);
                        }
                    }
                /*daily: {
                    axisTicks : {
                        show: false
                    },
                    axisBorder: {
                        show: false
                    },
                    min: 0,
                    max: 10,
                    tickAmount: 5,
                    labels    : {
                        offsetX: -9,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                            fontSize: '13px'
                        },
                        formatter: function(value) {
                            return '$'+parseFloat(value).toFixed(2);
                        }
                    }
                },
                weekly: {
                    axisTicks : {
                        show: false
                    },
                    axisBorder: {
                        show: false
                    },
                    min: 0,
                    max: 50,
                    tickAmount: 5,
                    labels    : {
                        offsetX: -9,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                            fontSize: '13px'
                        },
                        formatter: function(value) {
                            return '$'+parseFloat(value).toFixed(2);
                        }
                    }
                }*/
            }
        };
    }

}
