import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseConfigModule } from '@fuse/services/config';

import { adminConfig } from 'app/core/config/app.config';
import { adminRoutes } from './admin.routing';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
    declarations: [
    ],
    imports     : [
        RouterModule.forChild(adminRoutes),
        FuseConfigModule.forAdmin(adminConfig),
        SharedModule
    ]
})
export class AdminModule
{
}
