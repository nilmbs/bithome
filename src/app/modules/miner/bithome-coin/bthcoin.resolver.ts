import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { BithomeCoinService } from 'app/shared/services/bithome-coin.service';

@Injectable({
    providedIn: 'root'
})

export class BithomeCoinDataResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _bithomeCoinService: BithomeCoinService
    ) {
    }

    /**
     * Use this resolver to resolve initial mock-api for the application
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>
    {
        // Fork join multiple API endpoint calls to wait all of them to finish
        return forkJoin([
            this._bithomeCoinService.getData(),
            this._bithomeCoinService.getEarnings(),
            this._bithomeCoinService.getProfits()
        ]);
    }
}
