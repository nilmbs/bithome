import { Route} from '@angular/router';

import { BithomeCoinComponent } from './bithome-coin.component';
import { BithomeCoinDataResolver } from './bthcoin.resolver';

export const bithomeCoinRoutes: Route[] = [
    {
        path     : '',
        component: BithomeCoinComponent,
        resolve    : {
            initialData: BithomeCoinDataResolver
        }
    }
];

