import { Route} from '@angular/router';

import { MiningSettingComponent } from './mining-setting.component';

export const miningSettingRoutes: Route[] = [
    {
        path     : '',
        component: MiningSettingComponent
    }
];

