/* eslint-disable */
import * as moment from 'moment';

export const activities = {
    wallet : [
        {
            status: 1,
            currency: 'USDC',
            amount: 1.76,
            date: moment().subtract(1, 'days').format('MMMM DD, YYYY')
        },
         {
            status: 1,
            currency: 'HOME',
            amount: 5.034,
            date: moment().subtract(1, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 2,
            currency: 'USDC',
            amount: 50.00,
            date: moment().subtract(3, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'USDC',
            amount: 2.02,
            date: moment().subtract(3, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'USDC',
            amount: 1.56,
            date: moment().subtract(3, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'HOME',
            amount: 4.306,
            date: moment().subtract(3, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'USDC',
            amount: 3.22,
            date: moment().subtract(4, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 2,
            currency: 'HOME',
            amount: 25.06,
            date: moment().subtract(5, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'HOME',
            amount: 5.106,
            date: moment().subtract(5, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'USDC',
            amount: 1.89,
            date: moment().subtract(6, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'USDC',
            amount: 2.31,
            date: moment().subtract(6, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'HOME',
            amount: 2.106,
            date: moment().subtract(7, 'days').format('MMMM DD, YYYY')
        },
    ],

    bithomecoin : [
        /*{
            status: 1,
            currency: 'HOME',
            amount: 1.7678,
            date: moment().subtract(1, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 3,
            currency: 'HOME',
            amount: 5.106,
            date: moment().subtract(3, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'HOME',
            amount: 0.316,
            date: moment().subtract(4, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'HOME',
            amount: 1.371,
            date: moment().subtract(4, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 4,
            currency: 'HOME',
            amount: 4.316,
            date: moment().subtract(4, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'HOME',
            amount: 1.76,
            date: moment().subtract(6, 'days').format('MMMM DD, YYYY')
        },
        {
            status: 1,
            currency: 'HOME',
            amount: 2.76,
            date: moment().subtract(7, 'days').format('MMMM DD, YYYY')
        }*/
    ]
};
