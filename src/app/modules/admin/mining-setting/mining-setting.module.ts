import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { miningSettingRoutes } from './mining-setting.routing';
import { SharedModule } from 'app/shared/shared.module';
import { MiningSettingComponent } from './mining-setting.component';
import {MatMenuModule} from '@angular/material/menu';
@NgModule({
    declarations: [
        MiningSettingComponent
    ],
    imports     : [
        RouterModule.forChild(miningSettingRoutes),
        MatButtonToggleModule,
        NgApexchartsModule,
        SharedModule,
        MatMenuModule,
        MatIconModule
    ]
})

export class MiningSettingModule {
}
