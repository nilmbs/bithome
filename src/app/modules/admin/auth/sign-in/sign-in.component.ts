import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { FuseAdminConfigService } from '@fuse/services/config';
import { AppConfig } from 'app/core/config/app.config';
import { AuthService } from 'app/core/auth/auth.service';
import { IdleService } from 'app/shared/services/idle.service';

@Component({
    selector     : 'auth-admin-sign-in',
    templateUrl  : './sign-in.component.html',
    styleUrls: ["./sign-in.component.scss"],
    //encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AdminSignInComponent implements OnInit
{
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signInForm: FormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _fuseAdminConfigService: FuseAdminConfigService,
        private _idleService: IdleService,
        private _authService: AuthService
    ){}

    get config(): AppConfig {
        return this._fuseAdminConfigService.config;
    }

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signInForm = this._formBuilder.group({
            email     : ['', [Validators.required, Validators.email]],
            password  : ['', Validators.required],
            rememberMe: [false]
        });
    }

    /**
     * Sign in
     */
    signIn(): void
    {
        if ( this.signInForm.invalid ) {
            return;
        }

        this.signInForm.disable();
        this.showAlert = false;

        this._authService.adminSignIn(this.signInForm.value)
            .subscribe(resp => {
                this.signInForm.enable();
                this.signInForm.reset();
                if (resp.message) {
                    this.showAlert = true;
                    this.alert = {
                        type   : 'success',
                        message: resp.message
                    };
                }
                this._idleService.isSetLastAtvtm = true;
                const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/admin';
                this._router.navigateByUrl(redirectURL);
            },
            error => {
                this.signInForm.enable();
                this.showAlert = true;
                this.alert = {
                    type   : 'error',
                    message: error
                };
            }
        );
    }
}
