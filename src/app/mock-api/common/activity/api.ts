import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash-es';
import { FuseMockApiService } from '@fuse/lib/mock-api';
import { activities as activitiesData } from './data';

@Injectable({
    providedIn: 'root'
})
export class ActivityMockApi
{
    private _activities: any = activitiesData;

    /**
     * Constructor
     */
    constructor(private _fuseMockApiService: FuseMockApiService)
    {
        // Register Mock API handlers
        this.registerHandlers();
    }

    /**
     * Register Mock API handlers
     */
    registerHandlers(): void
    {
        this._fuseMockApiService
            .onGet('api/common/activities')
            .reply(() => [200, cloneDeep(this._activities)]);
    }
}
