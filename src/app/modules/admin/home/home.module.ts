import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { homeRoutes } from './home.routing';
import { SharedModule } from 'app/shared/shared.module';
import { HomeComponent } from './home.component';

@NgModule({
    declarations: [
       HomeComponent
    ],
    imports     : [
        RouterModule.forChild(homeRoutes),
        SharedModule,
        MatButtonToggleModule,
        MatProgressSpinnerModule
    ]
})
export class HomeModule
{
}
