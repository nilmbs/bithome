import { ChangeDetectionStrategy, OnInit, Component } from '@angular/core';

@Component({
    selector        : 'error-404',
    templateUrl     : './default.component.html',
    styleUrls       : ['./default.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

export class Error404Component implements OnInit {

    constructor()
    {
    }

    ngOnInit()
    {
    }
}
