import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseValidators } from '@fuse/validators';
import { FuseAlertType } from '@fuse/components/alert';
import { FuseConfigService } from '@fuse/services/config';
import { AppConfig } from 'app/core/config/app.config';
import { AuthService } from 'app/core/auth/auth.service';
import { ToasterService } from 'app/shared/services/toaster.service';

@Component({
    selector     : 'auth-reset-password',
    templateUrl  : './reset-password.component.html',
    styleUrls: ["./reset-password.component.scss"],
    //encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthResetPasswordComponent implements OnInit
{
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    resetPasswordForm: FormGroup;
    showAlert: boolean = false;
    email: string = '';
    token: string = '';

    /**
     * Constructor
     */
    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _formBuilder: FormBuilder,
        private toasterService:ToasterService,
        private _fuseConfigService: FuseConfigService,
        private _authService: AuthService
    ) {
        this.email = this._route.snapshot.paramMap.get('email');
        this.token = this._route.snapshot.paramMap.get('token');
    }

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.resetPasswordForm = this._formBuilder.group({
                password       : ['', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*]).{8,}$')]],
                passwordConfirm: ['', Validators.required]
            },
            {
                validators: FuseValidators.mustMatch('password', 'passwordConfirm')
            }
        );
        this._authService.verifyResetPasswordLink(this.email, this.token)
            .subscribe(resp => {
                if (resp.message) {
                    this.showAlert = true;
                    this.alert = {
                        type   : 'success',
                        message: resp.message
                    };
                }
            },
            error => {
                this.showAlert = true;
                this.alert = {
                    type   : 'error',
                    message: error
                };
            }
        );
    }

    get config(): AppConfig {
        return this._fuseConfigService.config;
    }

    /**
     * Reset password
     */
    resetPassword(): void
    {
        if ( this.resetPasswordForm.invalid ) {
            return;
        }

        this.resetPasswordForm.disable();
        this.showAlert = false;

        this._authService.resetPassword(this.email, this.token, this.resetPasswordForm.get('password').value)
            .subscribe(
                (resp) => {
                    this.resetPasswordForm.enable();
                    this.resetPasswordForm.reset();
                    if (resp.message == 'jwt expired') {
                        this.showAlert = true;
                        this.alert = {
                            type   : 'error',
                            message: 'Reset token has been expired.'
                        };
                    } else {
                        this.toasterService.openSnackBar('Your password has been reset.')
                        this._authService.signOut();
                        this._router.navigateByUrl('/sign-in');
                    }
                },
                (error) => {
                    this.resetPasswordForm.enable();
                    this.showAlert = true;
                    this.alert = {
                        type   : 'error',
                        message: 'Something went wrong, please try again.'
                    };
                }
            );
    }
}
