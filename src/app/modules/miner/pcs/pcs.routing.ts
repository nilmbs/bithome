import { Route} from '@angular/router';

import { PcsComponent } from './pcs.component';

export const pcsRoutes: Route[] = [
    {
        path     : '',
        component: PcsComponent
    }
];

