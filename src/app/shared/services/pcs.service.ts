import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError, timer } from 'rxjs';
import { catchError, mergeMap, switchMap, map, retry, share, delay } from 'rxjs/operators';
const EventSource: any = window['EventSource'];

import { HttpService } from './http.service';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class PcsService
{
    pcEventSources = {};

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpService,
        private _http: HttpClient
    ) {
    }

    /**
     * List all PCs
     *
     * @param email
     * @param token
     */
    getAll(email: string, token: string, intimer: boolean = false): Observable<any>
    {
        if (intimer)
            return timer(600 * 1000, 600 * 1000).pipe(
                    switchMap(() => this._httpClient.get(`mine/listpcs/${email}/${token}`).pipe(
                        map(response => response),
                        catchError(this.handleError)
                    )
                ),
                retry(4)
            );
        else
            return this._httpClient.get(`mine/listpcs/${email}/${token}`).pipe(
                map(response => response),
                catchError(this.handleError)
            );
    }

    /**
     * Set PC Active or Deactive
     *
     * @param device_id
     */
    saveStatus(email, device_id, status): Observable<any>
    {
        let post_data = {email: email, device: device_id, status: status};
        return this._httpClient.post('mine/updateminestatusweb', post_data).pipe(
            map(response => response),
            catchError(this.handleError)
        );
    }

    /**
     * Get all PC stats
     *
     * @param email
     * @param intervaltime (in seconds)
     */
    getAllPcStats(email: string, intervaltime: number = 300): Observable<any>
    {
        return timer(1, intervaltime * 1000).pipe(
                switchMap(() => this._httpClient.post('mine/workerinterval', {email: email}).pipe(
                    map(response => response),
                    catchError(this.handleError)
                )
            ),
            retry(4)
        );
    }

    /**
     * Get PC stats
     *
     * @param device_id
     */
    getStats(device_id: any): Observable<any>
    {
        return this._httpClient.get(`${environment.device_api_url}worker/${device_id}/currentStats`).pipe(
            map(response => response),
            catchError(this.handleError)
        );
    }

    /**
     * Register a PC
     *
     * @param data
     */
    registerPC(data): Observable<any>
    {
        let post_data = {email: data.email, mac_id: data.mac_id, pc_name: data.pc_name, gpu: data.gpu, coin: data.coin};
        return this._httpClient.post('mine/registerpc', post_data).pipe(
            map(response => response),
            catchError(this.handleError)
        );
    }

    checkNewPcAdded(): Observable<any> {
        const eventSource = this.getEventSource('http://localhost:5000/pcstart');
        this.pcEventSources['newpcadded'] = eventSource;

        return Observable.create(observer => {
            eventSource.onmessage = event => {
                observer.next(JSON.parse(event.data));
            };
            eventSource.onerror = event => {
                observer.error(event);
                //eventSource.close();
            };
        });
        return this._http.get('api/new-pc').pipe(
            map(response => response),
            catchError(this.handleError),
            delay(10000)
        );
    }

    private getEventSource(url: string): EventSource {
        return new EventSource(url);
    }

    private handleError(error) {
        let errorMessage = 'Something went wrong, please try again.';
        if (error.error && error.error.hasOwnProperty('message')) {
            errorMessage = error.error.message;
        } else if (error.status != 404 && error.message) {
            errorMessage = error.message;
        }

        return throwError(errorMessage);
    }
}
