import { Component, OnInit } from '@angular/core';

import { AdminMinerService } from 'app/shared/services/admin/miner.service';
import { User } from 'app/core/user/user.types';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
  	selector: 'app-dashboard',
  	templateUrl: './dashboard.component.html',
  	styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

    avgProfitChart: any = {};
  	private searchTimeout: any = null;
    keyword: string = '';
    allminers: User[] = [];
    filteredMiners: User[] = [];
    miners: User[] = [];
    listType: 'daily' | 'weekly' | 'monthly' = 'daily';
    totalPage: number = 0;
    currPage: number = 1;
    itemLimit: number = 6;
    loadMore: boolean = false;

    constructor(
        private _minerService: AdminMinerService,
        private _authService: AuthService
    ) {

    }

    ngOnInit(): void {
        this.getAllMiners();
        this._prepareChartData();
    }

    get activeMinersCount() {
        return this.allminers.reduce((acnt, miner:any) => {
            if (miner.status == 'Online') acnt++;
            return acnt;
        }, 0);
    }

    getAllMiners() {
        this._minerService.getList().subscribe(resp => {
            this.totalPage = Math.ceil(resp.length / this.itemLimit);
            this.allminers = resp;
            this.filteredMiners = resp;
            this.getMiners();
        });
    }

    getMiners() {
        let limit = this.currPage * this.itemLimit;
        this.filteredMiners.slice((this.currPage-1)*this.itemLimit, limit).filter(miner => {
            miner.devices = [];
            this._minerService.getListPCs(miner.email, this._authService.accessToken).subscribe(resp => {
                if (resp.devices.length > 0) {
                    miner.devices = resp.devices;
                }
            });
        });
        this.miners = this.filteredMiners.slice(0, limit);
        this.loadMore = false;
    }

    getDeviceIds(devices) {
        return devices.reduce((ids, pc:any) => { 
            ids.push(pc.device_id);
            return ids;
        }, []);
    }

    getActiveDeviceIds(devices) {
        return devices.reduce((ids, pc:any) => { 
            if (pc.is_active == 1) ids.push(pc.device_id);
            return ids;
        }, []);
    }

    searchMiners(evt: any) {
        clearTimeout(this.searchTimeout);
        let $this = this;
        this.keyword = evt.target.value;
        this.searchTimeout = setTimeout(() => {
            if (evt.keyCode != 13) {
                $this.findByKeyword();
            }
        }, 1000);
    }

    findByKeyword() {
        this.filteredMiners = this.allminers.filter(miner => miner.name.toLowerCase().includes(this.keyword.toLowerCase()));
        this.currPage = 1;
        this.totalPage = Math.ceil(this.filteredMiners.length / this.itemLimit);
        this.getMiners();
    }

    loadMoreMiners() {
        if (this.totalPage > this.currPage) {
            this.loadMore = true;
            this.currPage += 1;
            setTimeout(() => {
                this.getMiners();
            }, 1000);
        }
    }

    minersSortBy(skey: string, dir: string) {
        let direction = dir.toUpperCase();
        if (direction === 'ASC') {
            this.filteredMiners = this.filteredMiners.sort((a, b) => {
                if (a[skey] < b[skey]) {
                    return -1;
                } else if (a[skey] > b[skey]) {
                    return 1;
                }
                return 0;
            })
        } else if (direction === 'DESC') {
            this.filteredMiners = this.filteredMiners.sort((a, b) => {
                if (a[skey] > b[skey]) {
                    return -1;
                } else if (a[skey] < b[skey]) {
                    return 1;
                }
                return 0;
            })
        }
        //this.currPage = 1;
        this.getMiners();
    }

    private _prepareChartData() {

        this.avgProfitChart = {
            chart     : {
                animations: {
                    speed           : 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor : 'inherit',
                width     : '100%',
                height    : '100%',
                type      : 'area',
                toolbar   : {
                    show: false
                },
                zoom      : {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            fill      : {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    gradientToColors: [ '#e7fdf9'],
                    shadeIntensity: 0.15,
                    type: 'vertical',
                    opacityFrom: .21,
                    opacityTo: .12,
                    stops: [0, 100]
                },
            },
            grid      : {
                show       : true,
                borderColor: '#E4E4E6',
                position   : 'back',
                xaxis      : {
                    lines: {
                        show: true
                    }
                },
                yaxis      : {
                    lines: {
                        show: false
                    }
                }
            },
            markers: {
                size: 7,
                colors: '#FFF',
                strokeColors: '#00D091',
                strokeOpacity: 1,
                hover: {
                    size: 7
                },
            },
            stroke    : {
                width: 2,
                curve: 'straight',
                colors: ['#62EED3']
            },
            tooltip   : {
                shared      : false,
                intersect   : true,
                theme       : 'dark',
                custom: function({series, seriesIndex, dataPointIndex, w}) {
                    let value = parseFloat(series[seriesIndex][dataPointIndex]).toFixed(2);
                    return `<div class="flex items-center flex-col custom-tooltip">
                                <div class="label">${w.globals.lastXAxis.categories[dataPointIndex]}</div>
                                <div class="value">${value} coins</div>
                            </div>`
                }
            },
            series    : {
                daily: [{
                    name: 'Daily Mining',
                    data: [
                        21.03, 24.03, 18.04, 25.50, 26.34, 22.63, 23.75, 16.76, 19.30
                    ]
                }],
                weekly: [{
                    name: 'Weekly Mining',
                    data: [
                        29.5, 35.05, 30.56, 35.05, 30.34, 45.04, 40.46, 43.23, 35.43
                    ]
                }],
                monthly: [{
                    name: 'Monthly Mining',
                    data: [
                        40.5, 45.05, 38.56, 44.05, 30.34, 45.04, 40.46, 43.23, 35.43
                    ]
                }]
            },
            xaxis     : {
                daily: {
                    axisBorder: {
                        show: true,
                        color: '#CCC',
                    },
                    axisTicks : {
                        show: false
                    },
                    labels    : {
                        offsetY: 3,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                        }
                    },
                    tooltip   : {
                        enabled: false
                    },
                    crosshairs: {
                        show: false,
                    },
                    categories: ['01/13', '01/14', '01/15', '01/16', '01/17', '01/18', '01/19', '01/20', '01/21']
                },
                weekly: {
                    axisBorder: {
                        show: true,
                        color: '#CCC',
                    },
                    axisTicks : {
                        show: false
                    },
                    labels    : {
                        offsetY: 3,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                        }
                    },
                    tooltip   : {
                        enabled: false
                    },
                    crosshairs: {
                        show: false,
                    },
                    categories: ['11/20-11/26', '11/27-12/03', '12/04-12/10', '12/11-12/17', '12/18-12/24', '12/25-12/31', '01/01-01/07', '01/08-01/14', '01/15-01/21']
                },
                monthly: {
                    axisBorder: {
                        show: true,
                        color: '#CCC',
                    },
                    axisTicks : {
                        show: false
                    },
                    labels    : {
                        offsetY: 3,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                        },
                        formatter: function(value) {
                            let val = '';
                            if (!!value) {
                                val = value.replace(/\sAM/i, '').replace(/\sPM/i, '')
                            }
                            return val;
                        }
                    },
                    tickAmount: 8,
                    tooltip   : {
                        enabled: false
                    },
                    crosshairs: {
                        show: false,
                    },
                    categories: ["JUN'21", "JUL'21", "AUG'21", "SEP'21", "OCT'21", "NOV'21", "DEC'21", "JAN'22", "FEB'22"]
                }
            },
            yaxis     : {
                daily: {
                    axisTicks : {
                        show: false
                    },
                    axisBorder: {
                        show: false
                    },
                    min: 5,
                    max: 30,
                    tickAmount: 5,
                    labels    : {
                        offsetX: -9,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                            fontSize: '13px'
                        },
                        formatter: function(value) {
                            return parseFloat(value).toFixed(2);
                        }
                    }
                },
                weekly: {
                    axisTicks : {
                        show: false
                    },
                    axisBorder: {
                        show: false
                    },
                    min: 10,
                    max: 50,
                    tickAmount: 4,
                    labels    : {
                        offsetX: -9,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                            fontSize: '13px'
                        },
                        formatter: function(value) {
                            return parseFloat(value).toFixed(2);
                        }
                    }
                },
                monthly: {
                    axisTicks : {
                        show: false
                    },
                    axisBorder: {
                        show: false
                    },
                    min: 10,
                    max: 60,
                    tickAmount: 5,
                    labels    : {
                        offsetX: -9,
                        style  : {
                            colors: '#333',
                            fontWeight: 600,
                            fontSize: '13px'
                        },
                        formatter: function(value) {
                            return parseFloat(value).toFixed(2);
                        }
                    }
                }
            }
        };
    }
}
