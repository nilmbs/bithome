import { Component, OnInit } from '@angular/core';

import { MinerService } from 'app/shared/services/miner.service';
import { Activity } from 'app/shared/classes/activity';

@Component({
  	selector: 'app-earnings',
  	templateUrl: './earnings.component.html',
  	styleUrls: ['./earnings.component.scss']
})

export class EarningsComponent implements OnInit {
	recentActivities: Activity[] = [];
	totalPage: number = 0;
    currPage: number = 1;
    itemLimit: number = 6;
    loadMore: boolean = false;

  	constructor(private _minerService: MinerService) {}

  	ngOnInit(): void {
  		this.getRecentActivity();
  	}

  	getRecentActivity() {
  		let limit = this.currPage * this.itemLimit;
        this._minerService.getEarningRecentActivity().subscribe(resp => {
            this.totalPage = Math.ceil(resp.length / this.itemLimit);
            this.recentActivities = resp.slice(0, limit);
            this.loadMore = false;
        });
    }

    loadMoreActivities() {
    	if (this.totalPage > this.currPage) {
            this.loadMore = true;
            this.currPage += 1;
            setTimeout(() => {
                this.getRecentActivity();
            }, 1000);
        }
    }
}
