import { Route} from '@angular/router';

import { WalletComponent } from './wallet.component';

export const walletRoutes: Route[] = [
    {
        path     : '',
        component: WalletComponent
    }
];

