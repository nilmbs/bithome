import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { accountRoutes } from './account.routing';
import { SharedModule } from 'app/shared/shared.module';
import { AccountComponent } from './account.component';
import { AddEditWalletModalComponent } from './modal/add-edit-wallet/default.component';
import { DeleteWalletConfirmComponent } from './modal/delete-wallet/default.component';

@NgModule({
    declarations: [
        AccountComponent,
        AddEditWalletModalComponent,
        DeleteWalletConfirmComponent
    ],
    imports     : [
        RouterModule.forChild(accountRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        FuseAlertModule,
        MatProgressSpinnerModule,
        SharedModule
    ]
})
export class AccountModule
{
}
