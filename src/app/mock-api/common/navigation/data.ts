/* eslint-disable */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id      : 'main_nav',
        title   : '',
        subtitle: '',
        type    : 'group',
        icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'dashboard',
                title: 'Dashboard',
                type : 'basic',
                image: 'assets/images/icon-dashboard.svg',
                link : '/dashboard'
            },
            {
                id   : 'pcs',
                title: 'Pcs',
                type : 'basic',
                image: 'assets/images/icon-rigs.svg',
                link : '/pcs'
            },
            {
                id   : 'history',
                title: 'History',
                type : 'basic',
                image: 'assets/images/icon-history.svg',
                link : '/history'
            },
            // {
            //     id   : 'earnings',
            //     title: 'Earnings',
            //     type : 'basic',
            //     image: 'assets/images/icon-payouts.svg',
            //     link : '/earnings'
            // },
            {
                id   : 'wallet',
                title: 'Wallet',
                type : 'basic',
                image: 'assets/images/icon-payouts.svg',
                link : '/wallet'
            },
            {
                id   : 'bithome',
                title: 'Bithome Coin',
                type : 'basic',
                image: 'assets/images/icon-bithome-coin.svg',
                link : '/bithome-coin'
            },
            {
                id   : 'account',
                title: 'Account',
                type : 'basic',
                image: 'assets/images/icon-account.svg',
                link : '/account'
            },
            
        ]
    },
];
export const adminDefaultNavigation: FuseNavigationItem[] = [
    {
        id      : 'dashboard',
        title   : '',
        subtitle: '',
        type    : 'group',
        icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'admin_home',
                title: 'Home',
                type : 'basic',
                image: 'assets/images/icon-home.svg',
                link : '/admin/home'
            },
            {
                id   : 'admin_dashboard',
                title: 'Dashboard',
                type : 'basic',
                image: 'assets/images/icon-dashboard.svg',
                link : '/admin/dashboard'
            },
            {
                id   : 'admin_mining_settings',
                title: 'Mining Configuration',
                type : 'basic',
                image: 'assets/images/icon-mining.svg',
                link : '/admin/mining/settings'
            },
            
        ]
    },
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id      : 'dashboard',
        title   : 'Dashboard',
        tooltip : 'Dashboard',
        type    : 'aside',
        image   : 'assets/images/icon-dashboard.svg',
        link    : '/dashboard',
        children: []
    },
    {
        id      : 'pcs',
        title   : 'Pcs',
        tooltip : 'Pcs',
        type    : 'aside',
        image  : 'assets/images/icon-rigs.svg',
        link    : '/pcs',
        children: []
    },
    {
        id      : 'history',
        title   : 'History',
        tooltip : 'History',
        type    : 'aside',
        image   : 'assets/images/icon-history.svg',
        link    : '/history',
        children: []
    },
    // {
    //     id      : 'earnings',
    //     title   : 'Earnings',
    //     tooltip : 'Earnings',
    //     type    : 'aside',
    //     image   : 'assets/images/icon-payouts.svg',
    //     link    : '/earnings',
    //     children: []
    // },
    {
        id      : 'wallet',
        title   : 'Wallet',
        tooltip : 'Wallet',
        type    : 'aside',
        image   : 'assets/images/icon-payouts.svg',
        link    : '/wallet',
        children: []
    },
    {
        id      : 'bithome',
        title   : 'Bithome Coin',
        tooltip : 'Bithome Coin',
        type    : 'aside',
        image   : 'assets/images/icon-bithome-coin.svg',
        link    : 'bithome-coin',
        children: []
    },
    {
        id      : 'account',
        title   : 'Account',
        tooltip : 'Account',
        type    : 'basic',
        image   : 'assets/images/icon-account.svg',
        link    : '/account',
        children: []
    },
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id      : 'dashboards',
        title   : 'DASHBOARDS',
        type    : 'group',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'apps',
        title   : 'APPS',
        type    : 'group',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id   : 'others',
        title: 'OTHERS',
        type : 'group'
    },
    {
        id      : 'pages',
        title   : 'Pages',
        type    : 'aside',
        icon    : 'heroicons_outline:document-duplicate',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'user-interface',
        title   : 'User Interface',
        type    : 'aside',
        icon    : 'heroicons_outline:collection',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'navigation-features',
        title   : 'Navigation Features',
        type    : 'aside',
        icon    : 'heroicons_outline:menu',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id      : 'dashboards',
        title   : 'Dashboards',
        type    : 'group',
        icon    : 'heroicons_outline:home',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'apps',
        title   : 'Apps',
        type    : 'group',
        icon    : 'heroicons_outline:qrcode',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'pages',
        title   : 'Pages',
        type    : 'group',
        icon    : 'heroicons_outline:document-duplicate',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'user-interface',
        title   : 'UI',
        type    : 'group',
        icon    : 'heroicons_outline:collection',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'navigation-features',
        title   : 'Misc',
        type    : 'group',
        icon    : 'heroicons_outline:menu',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
