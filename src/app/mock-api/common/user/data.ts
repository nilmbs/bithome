/* eslint-disable */
export const user = {
    id    : 'cfaad35d-07a3-4447-a6c3-d8c3d54fd5df',
    name  : 'Brian Hughes',
    email : 'hughes.brian@company.com',
    avatar: 'assets/images/avatars/brian-hughes.jpg',
    status: 'online'
};

export const miners = [
	{
	    id    : 'cfaad35d-07a3-4000-a6c3-d8c3d54fd5df',
	    name  : 'Brian Hughes',
	    email : 'hughes.brian@company.com',
	    status: 'Online'
	},
	{
	    id    : 'cfaad35d-07a3-4007-a6c3-d8c3d54fd5df',
	    name  : 'Bithome Test1',
	    email : 'bithometest1@gmail.com',
	    status: 'Online'
	},
	{
	    id    : 'cfaad35d-07a3-4001-a6c3-d8c3d54fd5df',
	    name  : 'John Doe1',
	    email : 'john@company.com',
	    status: 'Online'
	},
	{
	    id    : 'cfaad35d-07a3-4002-a6c3-d8c3d54fd5df',
	    name  : 'John Doe2',
	    email : 'john@company.com',
	    status: 'Offline'
	},
	{
	    id    : 'cfaad35d-07a3-4003-a6c3-d8c3d54fd5df',
	    name  : 'John Doe3',
	    email : 'john@company.com',
	    status: 'Offline'
	},
	{
	    id    : 'cfaad35d-07a3-4004-a6c3-d8c3d54fd5df',
	    name  : 'John Doe4',
	    email : 'john@company.com',
	    status: 'Online'
	},
	{
	    id    : 'cfaad35d-07a3-4005-a6c3-d8c3d54fd5df',
	    name  : 'John Doe5',
	    email : 'john@company.com',
	    status: 'Offline'
	},
	{
	    id    : 'cfaad35d-07a3-4006-a6c3-d8c3d54fd5df',
	    name  : 'John Doe6',
	    email : 'john@company.com',
	    status: 'Offline'
	}
];
