import { Route } from '@angular/router';
import { AdminSignOutComponent } from './sign-out.component';

export const adminSignOutRoutes: Route[] = [
    {
        path     : '',
        component: AdminSignOutComponent
    }
];
