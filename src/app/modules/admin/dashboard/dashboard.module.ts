import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

import { dashboardRoutes } from './dashboard.routing';
import { SharedModule } from 'app/shared/shared.module';
import { DashboardComponent } from './dashboard.component';

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports     : [
        RouterModule.forChild(dashboardRoutes),
        MatButtonToggleModule,
        NgApexchartsModule,
        MatProgressSpinnerModule,
        SharedModule
    ]
})

export class DashboardModule {
}
