import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { FuseAlertType } from '@fuse/components/alert';
import { WalletService } from 'app/shared/services/wallet.service';

@Component({
    selector: 'add-edit-wallet-modal',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})
export class AddEditWalletModalComponent implements OnInit, OnDestroy {

    @ViewChild("addEditWalletModal", { static: false }) addEditWalletModal: TemplateRef<any>;

    public closeResult: string;
    public modalOpen: boolean = false;
    showAlert: boolean = false;
    walletForm: FormGroup;
    isEdit: boolean = false;
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private _walletService: WalletService
    ) { }

    ngOnInit(): void {
        this.walletForm = this.formBuilder.group({
            wallet_id: [''],
            wallet_name: ['', [Validators.required]],
            wallet_addr: ['', [Validators.required]],
        });
    }

    saveWallet() {
        if ( this.walletForm.invalid ) {
            return;
        }
        this.walletForm.disable();
        this.showAlert = false;
        var data = this.walletForm.getRawValue();

        this._walletService.addWallet('','','')
            .pipe(
                finalize(() => {
                    this.walletForm.enable();
                    this.showAlert = true;
                })
            )
            .subscribe(
                (resp) => {
                    this.walletForm.reset();
                    this.alert = {
                        type   : 'success',
                        message: 'Wallet added successfully.'
                    };
                },
                (error) => {
                    this.alert = {
                        type   : 'error',
                        message: error
                    };
                }
            );
    }

    openModal(wallet: any = '') {
        this.isEdit = false;
        this.walletForm.get('wallet_id').setValue(wallet);
        if (wallet) {
            this.isEdit = true;
        }
        this.walletForm.reset();
        this.modalOpen = true;
        if (isPlatformBrowser(this.platformId)) {
            this.modalService.open(this.addEditWalletModal, { 
                size: 'lg',
                ariaLabelledBy: 'addEditWallet-Modal',
                centered: true,
                windowClass: 'modal fade add-edit-wallet' 
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if(this.modalOpen){
            this.modalService.dismissAll();
        }
    }


}
