import { Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  Injectable, PLATFORM_ID, Inject, Output, EventEmitter } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { ToasterService } from 'app/shared/services/toaster.service';
import { WalletService } from 'app/shared/services/wallet.service';

@Component({
    selector: 'delete-wallet-confirm',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})

export class DeleteWalletConfirmComponent implements OnInit, OnDestroy {
    @ViewChild("deleteWalletModal", { static: false }) deleteWalletModal: TemplateRef<any>;

    @Output() deleted: EventEmitter<any> = new EventEmitter<any>();

    public closeResult: string;
    public modalOpen: boolean = false;
    wallet: any = '';

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private modalService: NgbModal,
        private toasterService:ToasterService,
        private _walletService: WalletService
    ) { }

    ngOnInit(): void {
    }

    deleteWallet() {
        this._walletService.deleteWallet('').subscribe(
            (resp) => {
                this.toasterService.openSnackBar('Wallet deleted successfully.', 'success');
                this.deleted.emit(this.wallet);
                this.modalService.dismissAll();
            },
            (error) => {
                this.toasterService.openSnackBar(error, 'error');
            }
        );
    }

    cancel() {
        this.modalService.dismissAll();
    }

    openModal(wallet: any = '') {
        this.wallet = wallet;

        this.modalOpen = true;
        if (isPlatformBrowser(this.platformId)) {
            this.modalService.open(this.deleteWalletModal, { 
                size: 'lg',
                ariaLabelledBy: 'deleteWallet-Modal',
                centered: true,
                windowClass: 'modal fade delete-wallet',
                backdrop: 'static'
            }).result.then((result) => {
                `Result ${result}`
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    ngOnDestroy() {
        if (this.modalOpen) {
            this.modalService.dismissAll();
        }
    }
}
