export interface Device {
	device_id: string;
	is_active: number;
    is_app: number;
	coin?: string;
    gpu: string;
    pc_name: string;
    started_date: string;
    web_active?: number;
    hash_rate?: number;
    estimate_daily?: number;
    estimate_weekly?: number;
    estimate_monthly?: number;
    last_seen?: number;
    temperature?: string;
    cpu_load?: string;
    scheduling?: boolean;
    game_detection?: boolean;
    is_optimized?: boolean;
}
