import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QRCodeModule } from 'angularx-qrcode';
import { ClipboardModule } from '@angular/cdk/clipboard';

// Modals
import { AddPcModalComponent } from './components/modal/add-pc/add-pc.component';
import { ChangeSchemaComponent } from './components/change-scheme/scheme.component';

// Pipes
import { SafeHtmlPipe } from './pipes/safehtml.pipe';
import { NewKeyValuePipe } from './pipes/newkeyvalue.pipe';
import { Currency0Pipe, Currency1Pipe, Currency2Pipe } from './pipes/currencies.pipe';

@NgModule({
    declarations: [
        AddPcModalComponent,
        ChangeSchemaComponent,
        SafeHtmlPipe,
        NewKeyValuePipe,
        Currency0Pipe,
        Currency1Pipe,
        Currency2Pipe
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        QRCodeModule,
        ClipboardModule
    ],
    exports: [        
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        QRCodeModule,
        ClipboardModule,
        AddPcModalComponent,
        ChangeSchemaComponent,
        SafeHtmlPipe,
        NewKeyValuePipe,
        Currency0Pipe,
        Currency1Pipe,
        Currency2Pipe
    ],
    providers: [
        CurrencyPipe,
        Currency0Pipe
    ]
})
export class SharedModule
{
}