import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { FuseCardModule } from '@fuse/components/card';
import { SharedModule } from 'app/shared/shared.module';
import { AdminSignOutComponent } from './sign-out.component';
import { adminSignOutRoutes } from './sign-out.routing';

@NgModule({
    declarations: [
        AdminSignOutComponent
    ],
    imports     : [
        RouterModule.forChild(adminSignOutRoutes),
        MatButtonModule,
        FuseCardModule,
        SharedModule
    ]
})
export class AdminSignOutModule
{
}
