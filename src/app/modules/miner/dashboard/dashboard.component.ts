import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { merge } from 'lodash-es';
import * as moment from 'moment';
import { ChartComponent } from "ng-apexcharts";

import { Currency0Pipe } from 'app/shared/pipes/currencies.pipe';
import { AddPcModalComponent} from "app/shared/components/modal/add-pc/add-pc.component";
import { PcsService } from 'app/shared/services/pcs.service';
import { AuthService } from 'app/core/auth/auth.service';
import { Device } from 'app/shared/classes/device';
import { MinerService } from 'app/shared/services/miner.service';
import { CommonUtils } from 'app/shared/utils/common.utils';
import { ToasterService } from 'app/shared/services/toaster.service';
import { Activity } from 'app/shared/classes/activity';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, OnDestroy {
    @ViewChild("addPcModal") addPcModal: AddPcModalComponent;

    private _unsubsPcAll: Subject<any> = new Subject<any>();
    private _unsubsPcStats: Subject<any> = new Subject<any>();
    recentActivities: Activity[] = [];
    all_pcs: Device[] = [];
    totalPc: number = 0;
    dailyProfitChartOpts: any = {};
    showLoader: boolean = true;
    showdfLoader: boolean = true;
    showRALoader: boolean = true;
    deviceExtKeys: any = {
        hash_rate: 0,
        estimate_daily: 0,
        estimate_weekly: 0,
        estimate_monthly: 0,
        last_seen: 0
    };

    constructor(
        private currencyPipe : Currency0Pipe,
        private _toasterService:ToasterService,
        private _minerService: MinerService,
        private _authService: AuthService,
        private _pcsService: PcsService
    ) { }

    async ngOnInit() {
        await this.getAllPcs();
        this.getRecentActivities();
        this._prepareChartData();
        /*this._pcsService.checkNewPcAdded().subscribe(
            (resp) => {
                //console.log(resp)
                if (!!resp.device) {
                    let index = this.all_pcs.findIndex((d:Device) => d.device_id == resp.device.device_id);
                    if (index < 0){
                        index = this.all_pcs.length;
                        let nitem: Device = resp.device;
                        let itemext = {
                            hash_rate: 45670,
                            estimate_daily: 25.678,
                            estimate_weekly: 25.678 * 7,
                            estimate_monthly: 750.564,
                            last_seen: 0,
                            temperature: '65°C',
                            cpu_load: '87%',
                            scheduling: true,
                            game_detection: false
                        };
                        nitem = merge(itemext, nitem);
                        this.all_pcs[index] = nitem;
                        this.totalPc = this.all_pcs.length;
                    }
                }
            },
            (error) => {
            }
        );*/
    }

    get favoritePcs() {
        return this.all_pcs.slice(0, 3);
        //return this.all_pcs.filter((pc:Device, index)=> ['default', 'rig0', 'rig2'].includes(pc.device_id));
    }

    get activeCount() {
        return this.all_pcs.reduce((acnt, pc: Device) => {
            if (pc.is_active == 1) acnt++;
            return acnt;
        }, 0);
    }

    get totalEstEarning() {
        let estearn1 =  this.all_pcs.reduce((amount, pc: Device) => {
            amount += pc.estimate_daily;
            return amount;
        }, 0);
        let estearn2 =  this.all_pcs.reduce((amount, pc: Device) => {
            amount += pc.estimate_weekly;
            return amount;
        }, 0);
        let estearn3 =  this.all_pcs.reduce((amount, pc: Device) => {
            amount += pc.estimate_monthly;
            return amount;
        }, 0);

        return {
            daily: estearn1,
            weekly: estearn2,
            monthly: estearn3
        };
    }

    getRecentActivities() {
        this._minerService.getRecentActivity().subscribe(resp => {
            resp.data.forEach((item, index) => {
                if (index > 4) return;
                const aitem = item.earning;
                let activity = {
                    status: (aitem.value=='add'?1:2),
                    currency: aitem.currency,
                    amount: aitem.earning,
                    date: moment.unix(aitem.endDate).format('MMMM DD, YYYY'),
                    est_amount: (aitem.currency=='HOME'?aitem.earning*.05:aitem.earning)
                };
                this.recentActivities.push(activity);
            });
            this.showRALoader = false;
        });
    }

    async getAllPcs() {
        let user = this._authService.user;

        try {
            let resp = await this._pcsService.getAll(user.email, this._authService.accessToken).toPromise();
            this.all_pcs = resp.devices;
            this.totalPc = this.all_pcs.length;
            if (this.totalPc <= 0) {
                this.showLoader = false;
                return;
            }
            this.all_pcs.filter((item: Device, index) => {
                item = merge(item, this.deviceExtKeys);
            });
            this._pcsService.getAll(user.email, this._authService.accessToken, true)
                .pipe(takeUntil(this._unsubsPcAll))
                .subscribe((resp) => {
                    resp.devices.filter((item: Device, index) => {
                        const pcindex = this.all_pcs.findIndex(p => p.device_id == item.device_id);
                        let pcitem = this.all_pcs[pcindex];
                        if (pcitem) {
                            pcitem = merge(pcitem, item);
                            if (this.pcAppSts(item) != 0 && item.is_active == 0) {
                                delete this.deviceExtKeys.last_seen;
                                pcitem = merge(pcitem, this.deviceExtKeys);
                            }
                            this.all_pcs[pcindex] = pcitem;
                        }
                    });
            });
            this._pcsService.getAllPcStats(user.email, 600)
                .pipe(takeUntil(this._unsubsPcStats))
                .subscribe(
                    (stsresp) => {
                        this.all_pcs.filter((item: Device, index) => {
                            let pcsts = stsresp.data.find(w => w.worker == item.device_id);
                            this.updatePcsStats(pcsts, item);
                            this._pcsService.getStats(item.device_id).subscribe(
                                (resp) => {
                                    //console.log(resp)
                                    if (resp.status != 'OK' || typeof resp.data != 'object') return;
                                    this.updatePcsStats(resp.data, item, true);
                                },
                                (error) => {
                                }
                            );
                        });
                        this.showLoader = false;
                    },
                    (error) => {
                        this.showLoader = false;
                    }
                );
        } catch (error) {
            this.showLoader = false;
        }
    }

    addPc() {
        let user = this._authService.user;
        let postdata = {
            email: user.email,
            mac_id: Math.floor(Math.random() * (999999 - 100000)),
            pc_name: 'TEST PC',
            gpu: 'AMD Radeon RX 5700 XT 8GB',
            coin: 'Ethereum'
        };
        this._pcsService.registerPC(postdata).subscribe(
            (resp) => {
                this.getAllPcs();
            },
            (error) => {
                
            }
        );
    }

    startMining(device: any) {
        if (this.pcAppSts(device) == 0) return;
        this._pcsService.saveStatus(this._authService.user.email, device.device_id, 1).subscribe(
            (resp) => {
                if (resp.status == 1) {
                    this.all_pcs.filter((item: Device, index) => {
                        if (item.device_id === device.device_id) {
                            item.is_active = 1;
                        }
                    });
                    this._toasterService.openSnackBar('Mining started successfully.', 'success');
                }
            },
            (error) => {
                //this._toasterService.openSnackBar(error, 'error')
            }
        );
    }

    stopMining(device: Device) {
        if (this.pcAppSts(device) == 0) return;
        this._pcsService.saveStatus(this._authService.user.email, device.device_id, 2).subscribe(
            (resp) => {
                if (resp.status == 2) {
                    this.all_pcs.filter((item: Device, index) => {
                        if (item.device_id === device.device_id) {
                            item.is_active = 0;
                        }
                    });
                    this._toasterService.openSnackBar('Mining stopped successfully.', 'success');
                }
            },
            (error) => {
                //this._toasterService.openSnackBar(error, 'error')
            }
        );
    }

    chkIsMining(device) {
        return device.is_app != 0 && device.is_active == 1;
    }

    pcAppSts(device) {
        let appsts = device.is_app || 0;
        return appsts;
    }

    private updatePcsStats(pcsts, item, single = false) {
        if (!pcsts) {
            item.is_active = 0;
            item.hash_rate = 0;
            if (!single) {
                item.estimate_daily = 0;
                item.estimate_weekly = 0;
                item.estimate_monthly = 0;
            }
            return;
        }
        let itemActive = false;
        const rhashRate = pcsts.reportedHashrate || 0;
        if (+item.last_seen > 0) {
            let prevLastSeen = moment.unix(+item.last_seen);
            let lastSeen = moment.unix(+pcsts.lastSeen);
            if (lastSeen.diff(prevLastSeen, 'minutes') > 15) {
                itemActive = false;
            } else  if (rhashRate > 0) {
                itemActive = true;
            }
        }
        if (rhashRate > 0 && +item.last_seen == 0) itemActive = true;
        //if (rhashRate <= 0 && item.is_active == 1) item.is_active = 0;
        if (itemActive) {
            item.hash_rate = pcsts.currentHashrate || 0;
            if (!single) {
                item.estimate_daily = pcsts.estimateDay || 0;
                item.estimate_weekly = (pcsts.estimateDay || 0) * 7;
                item.estimate_monthly = pcsts.estimateMonth || 0;
            }
        } else {
            item.hash_rate = 0;
            if (!single) {
                item.estimate_daily = 0;
                item.estimate_weekly = 0;
                item.estimate_monthly = 0;
            }
        }
        if (!!pcsts.lastSeen) item.last_seen = pcsts.lastSeen;
    }

    async _prepareChartData() {
        const $this = this;
        let edata = [];
        if (this.totalPc > 0) {
            try {
                const resp = await this._minerService.getDailyProfitDashboard().toPromise();
                let dailyearn = CommonUtils.sortData(resp.data, 'startDate', 'asc');
                let weekStart = moment().startOf('week');
                for (let i = 0; i <= 6; i++) {
                    let mdt = moment(weekStart).add(i, 'days');
                    let ernitem = dailyearn.find((item) => item.startDate === mdt.unix()) || {earning: 0};
                    edata.push(ernitem.earning);
                };
            } catch (error) {}
        }

        this.showdfLoader = false;
        this.dailyProfitChartOpts = {
            chart      : {
                fontFamily: 'inherit',
                foreColor : 'inherit',
                height    : '100%',
                type      : 'bar',
                toolbar   : {
                    show: false
                },
            },
            colors     : ['#00d091'],
            dataLabels : {
                enabled        : true,
                enabledOnSeries: [0],
                offsetY: -30,
                style: {
                    colors: ['#000']
                },
                formatter: function (value) {
                    return $this.currencyPipe.transform(value);
                }
            },
            grid       : {
                show: false
            },
            labels     : ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            legend     : {
                show: false
            },
            plotOptions: {
                bar: {
                    columnWidth: '50px',
                    borderRadius: 10,
                    dataLabels: {
                      position: 'top'
                    }
                },
            },
            series     : [{
                data: edata,
                name: 'Daily Profit'
            }],
            states     : {
                hover: {
                    filter: {
                        type : 'none'
                    }
                },
                active: {
                    filter: {
                        type : 'none'
                    }
                }
            },
            tooltip    : {
                enabled: false
            },
            xaxis      : {
                axisTicks: {
                    show: true
                },
                axisBorder: {
                    show: true
                },
                labels    : {
                    rotate: 90,
                    rotateAlways: true,
                    offsetY: 30,
                    style: {
                        colors: '#000'
                    }
                }
            },
            yaxis      : {
                labels: {
                    show: false
                }
            }
        };
    }

    ngOnDestroy() {
        this._unsubsPcAll.next();
        this._unsubsPcAll.complete();
        this._unsubsPcStats.next();
        this._unsubsPcStats.complete();
    }
}
