import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseValidators } from '@fuse/validators';
import { FuseAlertType } from '@fuse/components/alert';
import { FuseConfigService } from '@fuse/services/config';

import { AppConfig } from 'app/core/config/app.config';
import { AddEditWalletModalComponent } from './modal/add-edit-wallet/default.component';
import { DeleteWalletConfirmComponent } from './modal/delete-wallet/default.component';
import { MinerService } from 'app/shared/services/miner.service';
import { WalletService } from 'app/shared/services/wallet.service';
import { User } from 'app/core/user/user.types';

@Component({
  	selector: 'app-account',
  	templateUrl: './account.component.html',
  	styleUrls: ['./account.component.scss']
})

export class AccountComponent implements OnInit {
	@ViewChild("addEditWalletModal") AddEditWalletModal: AddEditWalletModalComponent;
	@ViewChild("deleteWalletConfirm") DeleteWalletConfirm: DeleteWalletConfirmComponent;
	user: User;
	alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    showAlert: boolean = false;
    changePasswordForm: FormGroup;
    wallets: any[] = [];

  	constructor(
  		private _formBuilder: FormBuilder,
        private _fuseConfigService: FuseConfigService,
  		private _minerService: MinerService,
        private _walletService: WalletService
  	) { }

  	ngOnInit(): void {
  		this.user = this._minerService.miner;
  		this.changePasswordForm = this._formBuilder.group({
  				currentPwd: ['', Validators.required],
                password       : ['', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*]).{8,}$')]],
                passwordConfirm: ['', Validators.required]
            },
            {
                validators: FuseValidators.mustMatch('password', 'passwordConfirm')
            }
        );
        this.getWallets();
  	}

    get config(): AppConfig {
        return this._fuseConfigService.config;
    }

    getWallets() {
        this._walletService.getAll().subscribe((resp) => this.wallets = resp);
    }

    walletAddedOrUpdated(result: any) {
        let wallet = result.wallet;
        if (result.added) {
            wallet.wallet_id = 1000 + (this.wallets.length+1);
            this.wallets.push(result.wallet);
        } else if (result.updated) {
            this.wallets.filter((w: any, index) => {
                if (w.wallet_id == wallet.wallet_id) {
                    w.nickname = wallet.nickname;
                    w.address = wallet.address;
                    return true;
                }
            });
        }
        
    }

    walletDeleted(wallet: any) {
        let index = this.wallets.indexOf(wallet);
        this.wallets.splice(index, 1);
    }

  	/**
     * Change to new password
     */
    changePassword(): void
    {
        if ( this.changePasswordForm.invalid ) {
        	let frmctrls = this.changePasswordForm.controls;
            for (let fkey in frmctrls) {
            	let field = frmctrls[fkey];
                if (!field.valid)
                    field.markAsTouched()
                else
                    field.markAsUntouched()
            }
            return;
        }
        if (this.changePasswordForm.get('currentPwd').value == this.changePasswordForm.get('password').value) {
            this.showAlert = true;
            this.alert = {
                type   : 'error',
                message: 'New password can not be same as current password.'
            };
            return;
        }

        this.changePasswordForm.disable();
        this.showAlert = false;

        let postdata = {
            token: this.user.token,
            email: this.user.email,
            curr_password: this.changePasswordForm.get('currentPwd').value,
            password: this.changePasswordForm.get('password').value,
        };
        this._minerService.changePassword(postdata)
            .subscribe(
                (resp) => {
                    this.changePasswordForm.enable();
                    this.changePasswordForm.reset();
                    this.showAlert = true;
                    this.alert = {
                        type   : 'success',
                        message: 'Your password updated successfully.'
                    };
                },
                (error) => {
                    this.changePasswordForm.enable();
                    this.showAlert = true;
                    this.alert = {
                        type   : 'error',
                        message: error || 'Something went wrong, please try again.'
                    };
                }
            );
    }

    setTwoFactorIdentify(evt: any): void
    {
        let acc_tfidnf: boolean = false;
        if (evt.target.checked) acc_tfidnf = true;
        this._fuseConfigService.config = {acc_tfidnf};
    }
}
